package algoritmos;

import implementaciones.ConjuntoDinamico;
import tdas.ColaTDA;
import tdas.ConjuntoTDA;
import tdas.PilaTDA;

public class EjercicioConjunto
{

	public static boolean sonLosMismosElementos(PilaTDA pila, ColaTDA cola)
	{
		ConjuntoTDA aux = new ConjuntoDinamico();
		aux.inicializar();
		
		while (!pila.estaVacia())
		{
			aux.agregar(pila.tope());
			pila.desapilar();
		}
		
		boolean mismos = true;
		
		while (mismos && !cola.estaVacia())
		{
			if (!aux.pertenece(cola.primero()))
			{
				mismos = false;
			}
			cola.desacolar();
		}	
		
		return mismos;
	}

	public static ConjuntoTDA generarAPartirDe(PilaTDA pila, ColaTDA cola) 
	{
		ConjuntoTDA c = new ConjuntoDinamico();
		c.inicializar();
		
		ConjuntoTDA aux = new ConjuntoDinamico();
		aux.inicializar();
		
		while (!pila.estaVacia())
		{
			aux.agregar(pila.tope());
			pila.desapilar();
		}
		
		while (!cola.estaVacia())
		{
			int elemento = cola.primero();
			
			if (aux.pertenece(elemento))
			{
				c.agregar(elemento);
			}
			
			cola.desacolar();
		}
		
		return c;
	}

	public static int cantidadElementos(ConjuntoTDA c)
	{
		int cantidad = 0;
		
		while (!c.estaVacio())
		{
			cantidad++;
			c.sacar(c.elegir());
		}
		
		return cantidad;
	}

	public static boolean sonIguales(ConjuntoTDA c1, ConjuntoTDA c2)
	{
		boolean sonIguales = true;
		
		while (sonIguales &&
			   !c1.estaVacio() &&
			   !c2.estaVacio())
		{
			int elemento = c1.elegir();
			
			if (!c2.pertenece(elemento))
			{
				sonIguales = false;
			}
			
			c1.sacar(elemento);
			c2.sacar(elemento);
		}
		
		if (!c1.estaVacio() || !c2.estaVacio())
		{
			sonIguales = false;
		}
		
		return sonIguales;
	}

}
