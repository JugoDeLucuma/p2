package algoritmos;

import implementaciones.ColaDinamica;
import implementaciones.ConjuntoDinamico;
import implementaciones.ConjuntoEstatico;
import implementaciones.PilaDinamica;
import tdas.ColaTDA;
import tdas.ConjuntoTDA;
import tdas.PilaTDA;

public class EjerciciosPila {

	public static PilaTDA pasarUnaPilaAOtraInvertida(PilaTDA auxiliar)
	{
		PilaTDA invertida = new PilaDinamica();
		invertida.inicializar();
		
		while (!auxiliar.estaVacia())
		{
			invertida.apilar(auxiliar.tope());
			auxiliar.desapilar();
		}
		
		return invertida;
	}

	public static int contarElementos(PilaTDA original)
	{
		int cantidad = 0;
		
		while(!original.estaVacia())
		{
			cantidad++;
			original.desapilar();
		}
		
		return cantidad;
	}

	public static double promedio(PilaTDA original)
	{
		int suma = 0;
		int cantidad = 0;
		
		while(!original.estaVacia())
		{
			suma+= original.tope();
			cantidad++;
			original.desapilar();
		}
		
		return suma/cantidad;
	}

	public static int sumaDeElementos(PilaTDA original)
	{
		int suma = 0;
		
		while(!original.estaVacia())
		{
			suma+= original.tope();
			original.desapilar();
		}
		
		return suma;
	}

	public static boolean esCapicua(PilaTDA pila)
	{
		boolean esCapicua = true;
		int cantidadElementos = 0;
		
		ColaTDA cola = new ColaDinamica();
		cola.inicializar();
		
		while (!pila.estaVacia())
		{
			cola.acolar(pila.tope());
			cantidadElementos++;
			pila.desapilar();
		}
		
		int mitad = cantidadElementos/2;
		
		PilaTDA aux = new PilaDinamica();
		aux.inicializar();
		
		while (!cola.estaVacia() && mitad > 0)
		{
			aux.apilar(cola.primero());
			cola.desacolar();
			mitad--;
		}
		
		if (cantidadElementos % 2 != 0)
		{
			cola.desacolar();
		}
		
		while (esCapicua && !cola.estaVacia() && !aux.estaVacia())
		{
			if (cola.primero() != aux.tope())
			{
				esCapicua = false;
			}
			
			cola.desacolar();
			aux.desapilar();
		}
				
		return esCapicua;
	}

	public static void eliminarRepeticiones(PilaTDA pila)
	{
		PilaTDA invertida = EjerciciosPila.pasarUnaPilaAOtraInvertida(pila);
		
		ConjuntoTDA c = new ConjuntoDinamico();
		c.inicializar();
		
		while (!invertida.estaVacia())
		{
			int elemento = invertida.tope();
			
			if (!c.pertenece(elemento))
			{
				c.agregar(elemento);
				pila.apilar(elemento);
			}
			
			invertida.desapilar();
		}
	}

	public static ConjuntoTDA elementosRepetidos(PilaTDA pila)
	{
		ConjuntoTDA repetidos = new ConjuntoDinamico();
		repetidos.inicializar();
		
		ConjuntoTDA auxiliar = new ConjuntoEstatico();
		auxiliar.inicializar();
		
		while (!pila.estaVacia())
		{
			if (!auxiliar.pertenece(pila.tope()))
			{
				auxiliar.agregar(pila.tope());
			}
			else
			{
				repetidos.agregar(pila.tope());
			}
			
			pila.desapilar();
		}
				
		return repetidos;
	}

}
