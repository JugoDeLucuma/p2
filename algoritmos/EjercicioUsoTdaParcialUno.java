package algoritmos;

import implementaciones.ConjuntoDinamico;
import tdas.ABBTDA;
import tdas.ConjuntoTDA;

public class EjercicioUsoTdaParcialUno 
{
	public static boolean determinarSiAlgunoDeLosElementosCoincideConLaCantidad(ConjuntoTDA c)
	{
		int cantidadDeElementos = cantidadElementosConjunto(c);
		boolean coincide = false;
				
		while (!coincide && !c.estaVacio())
		{	
			int elemento = c.elegir();
			
			if (cantidadDeElementos == elemento)
			{
				coincide = true;
			}
			
			c.sacar(elemento);
		}
		
		return coincide;
	}

	private static int cantidadElementosConjunto(ConjuntoTDA c)
	{
		ConjuntoTDA aux = new ConjuntoDinamico();
		aux.inicializar();
		int cantidad = 0;
		
		while (!c.estaVacio())
		{
			cantidad++;
			int elemento = c.elegir();
			aux.agregar(elemento);
			c.sacar(elemento);
		}
		
		while (!aux.estaVacio())
		{
			int elemento = aux.elegir();
			c.agregar(elemento);
			aux.sacar(elemento);
		}
		
		return cantidad;
	}
	
	public static  int cuantosNodoTienenUnSoloHijo(ABBTDA a)
	{
		if (a.estaVacio() || (a.hijoDerecho().estaVacio() && a.hijoIzquierdo().estaVacio()))
		{
			return 0;
		}
		else
		{
			if ((!a.hijoDerecho().estaVacio() &&
				 a.hijoIzquierdo().estaVacio()) ||
			     (a.hijoDerecho().estaVacio() && !a.hijoIzquierdo().estaVacio()))
			{
				return 1;
			}
			else
			{
				return cuantosNodoTienenUnSoloHijo(a.hijoIzquierdo()) + 
					   cuantosNodoTienenUnSoloHijo(a.hijoDerecho());
			}
		}
	}
}
