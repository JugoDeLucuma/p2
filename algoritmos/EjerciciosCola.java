package algoritmos;

import implementaciones.ColaDinamica;
import implementaciones.ConjuntoDinamico;
import implementaciones.PilaDinamica;
import tdas.ColaTDA;
import tdas.ConjuntoTDA;
import tdas.PilaTDA;

public class EjerciciosCola {

	public static ColaTDA pasarColaAOtra(ColaTDA original)
	{
		ColaTDA duplicada = new ColaDinamica();
		duplicada.inicializar();
		
		while (!original.estaVacia())
		{
			duplicada.acolar(original.primero());
			original.desacolar();
		}
		
		return duplicada;
	}

	public static ColaTDA invertirColaUsandoPilas(ColaTDA original)
	{
		ColaTDA invertida = new ColaDinamica();
		invertida.inicializar();
		
		PilaTDA aux = new PilaDinamica();
		aux.inicializar();
		
		while (!original.estaVacia())
		{
			aux.apilar(original.primero());
			original.desacolar();
		}
		
		while (!aux.estaVacia())
		{
			invertida.acolar(aux.tope());
			aux.desapilar();
		}
		
		return invertida;
	}

	public static boolean determinarSiElFinalDeUnaColaEsIgualAlDeOtra(ColaTDA cola1, ColaTDA cola2)
	{
		int ultimoValorCola1 = 0;
		int ultimoValorCola2 = 0;
		
		if (cola1.estaVacia() || cola2.estaVacia())
		{
			return false;
		}
		
		while (!cola1.estaVacia() && !cola2.estaVacia())
		{
			ultimoValorCola1 = cola1.primero();
			ultimoValorCola2 = cola2.primero();
			
			cola1.desacolar();
			cola2.desacolar();
		}
		
		while (!cola1.estaVacia())
		{
			ultimoValorCola1 = cola1.primero();
			cola1.desacolar();
		}
		
		while (!cola2.estaVacia())
		{
			ultimoValorCola2 = cola2.primero();
			cola2.desacolar();
		}
				
		return ultimoValorCola1 == ultimoValorCola2;
	}

	public static boolean esCapicua(ColaTDA cola)
	{
		ColaTDA aux = new ColaDinamica();
		aux.inicializar();
		
		int cantidad = 0;
		
		while (!cola.estaVacia())
		{
			aux.acolar(cola.primero());
			cantidad++;
			cola.desacolar();
		}
		
		PilaTDA pila = new PilaDinamica();
		pila.inicializar();	
		
		int mitad = cantidad / 2;
		
		while (!aux.estaVacia() && mitad > 0)
		{
			pila.apilar(aux.primero());
			aux.desacolar();
			mitad--;
		}
		
		if ((cantidad % 2) != 0)
		{
			aux.desacolar();
		}
		
		boolean capicua = true;
		
		while (capicua && !aux.estaVacia() && !pila.estaVacia())
		{
			if (aux.primero() != pila.tope())
			{
				capicua = false;
			}
			aux.desacolar();
			pila.desapilar();
		}
		
		return capicua;
	}

	public static boolean sonInversas(ColaTDA cola1, ColaTDA cola2)
	{
		PilaTDA pila = new PilaDinamica();
		pila.inicializar();
		
		while (!cola1.estaVacia())
		{
			pila.apilar(cola1.primero());
			cola1.desacolar();
		}
		
		boolean sonInversas = true;
		
		while (sonInversas && !cola2.estaVacia() && !pila.estaVacia())
		{
			if (cola2.primero() != pila.tope())
			{
				sonInversas = false;
			}
			
			cola2.desacolar();
			pila.desapilar();
		}
		
		if (!cola2.estaVacia() || !pila.estaVacia())
		{
			sonInversas = false;
		}
		
		return sonInversas;
	}

	public static void sacarRepetidos(ColaTDA cola)
	{
		ConjuntoTDA c = new ConjuntoDinamico();
		c.inicializar();
		
		ColaTDA aux = new ColaDinamica();
		aux.inicializar();
		
		while (!cola.estaVacia())
		{
			int elemento = cola.primero();
			
			if (!c.pertenece(elemento))
			{
				c.agregar(elemento);
				aux.acolar(elemento);
			}
			
			cola.desacolar();
		}
		
		while (!aux.estaVacia())
		{
			cola.acolar(aux.primero());
			aux.desacolar();
		}
		
	}

	public static ConjuntoTDA generarConjuntoRepetidos(ColaTDA cola)
	{
		ConjuntoTDA repetidos = new ConjuntoDinamico();
		repetidos.inicializar();
		ConjuntoTDA todos = new ConjuntoDinamico();
		todos.inicializar();
		
		while (!cola.estaVacia())
		{
			int elemento = cola.primero();
			
			if (!todos.pertenece(elemento))
			{
				todos.agregar(elemento);
			}
			else
			{
				repetidos.agregar(elemento);
			}
			
			cola.desacolar();
		}
		
		return repetidos;
	}

}
