package algoritmos;

import implementaciones.ConjuntoDinamico;
import tdas.ABBTDA;
import tdas.ConjuntoTDA;

public class EjerciciosArbolBB 
{
	public static boolean esta(ABBTDA arbol, int i)
	{
		if (arbol.estaVacio())
		{
			return false;
		}
		else
		{
			if (arbol.raiz() == i)
			{
				return true;
			}
			else
			{
				if (i > arbol.raiz())
				{
					return esta(arbol.hijoDerecho(),i);
				}
				else
				{
					return esta(arbol.hijoIzquierdo(),i);
				}
			}
		}
	}

	public static boolean esUnaHoja(ABBTDA arbol, int i)
	{
		if (arbol.estaVacio())
		{
			return false;
		}
		else
		{
			if (arbol.raiz() == i)
			{
				return arbol.hijoDerecho().estaVacio() &&
					   arbol.hijoIzquierdo().estaVacio();
			}
			else
			{
				if (i > arbol.raiz())
				{
					return esUnaHoja(arbol.hijoDerecho(),i);
				}
				else
				{
					return esUnaHoja(arbol.hijoIzquierdo(),i);
				}
			}
		}
	}

	public static int profundidad(ABBTDA arbol, int i)
	{
		if (arbol.estaVacio())
		{
			return 0;
		}
		else
		{
			if (arbol.raiz() == i)
			{
				return 0;
			}
			else
			{
				if (i > arbol.raiz())
				{
					return 1 + profundidad(arbol.hijoDerecho(), i);
				}
				else
				{
					return 1 + profundidad(arbol.hijoIzquierdo(), i);
				}
			}
		}
	}

	public static int menorValor(ABBTDA arbol)
	{
		if (arbol.hijoIzquierdo().estaVacio())
		{
			return arbol.raiz();
		}
		else
		{
			return menorValor(arbol.hijoIzquierdo());
		}
	}

	public static int cantidadElementos(ABBTDA arbol)
	{
		if (arbol.estaVacio())
		{
			return 0;
		}
		else
		{
			return 1 + cantidadElementos(arbol.hijoIzquierdo()) +
					   cantidadElementos(arbol.hijoDerecho());
		}
	}

	public static int sumaElementos(ABBTDA arbol)
	{
		if (arbol.estaVacio())
		{
			return 0;
		}
		else
		{
			return arbol.raiz() +
				   sumaElementos(arbol.hijoIzquierdo()) +
				   sumaElementos(arbol.hijoDerecho());
		}
	}

	public static int cantidadHojas(ABBTDA arbol)
	{
		if (arbol.estaVacio())
		{
			return 0;
		}
		else
		{
			if (arbol.hijoIzquierdo().estaVacio() && 
				arbol.hijoDerecho().estaVacio())
			{
				return 1;
			}
			else
			{
				return cantidadHojas(arbol.hijoDerecho()) +
					   cantidadHojas(arbol.hijoIzquierdo()); 
			}
		}
	}

	public static int altura(ABBTDA arbol)
	{
		if (!arbol.estaVacio())
		{
			if (arbol.hijoIzquierdo().estaVacio() &&
				arbol.hijoDerecho().estaVacio())
			{
				return 0;
			}
			else
			{
				return 1 +
				max(altura(arbol.hijoIzquierdo()),
					altura(arbol.hijoDerecho()));
			}
		}
		
		return 0;
	}
	
	private static int max(int a, int b)
	{
		if (a < b)
		{
			return b;
		}
		else
		{
			return a;
		}
	}
	
	public static int cantidadElementosMayores(ABBTDA a,int x)
	{
		if (a.estaVacio())
		{
			return 0;
		}
		else
		{
			if (a.raiz() > x)
			{
				return 1 +
				cantidadElementosMayores(a.hijoIzquierdo(), x) +
				cantidadElementosMayores(a.hijoDerecho(),x);
			}
			else
			{
				return  cantidadElementosMayores(a.hijoIzquierdo(), x) +
						cantidadElementosMayores(a.hijoDerecho(),x);
			}
		}
		
	}
	
	public static ConjuntoTDA nodosPares(ABBTDA arbol)
	{
		ConjuntoTDA r = new ConjuntoDinamico();
		r.inicializar();

		if (!arbol.estaVacio())
		{
			if (arbol.raiz () % 2 == 0)
			{
				r.agregar(arbol.raiz());
			}

			ConjuntoTDA rI = nodosPares(arbol.hijoIzquierdo());
			ConjuntoTDA rD = nodosPares(arbol.hijoDerecho());

			while (!rI.estaVacio())
			{
				int x = rI.elegir();
				r.agregar(x);
				rI.sacar(x);
			}
			
			while (!rD.estaVacio())
			{
				int x = rD.elegir();
				r.agregar(x);
				rD.sacar(x);
			}

		}

		return r;
	}

}
