package algoritmos;

import implementaciones.ColaPrioridadDinamica;
import implementaciones.ConjuntoDinamico;
import tdas.ColaPrioridadTDA;
import tdas.ConjuntoNotasTDA;
import tdas.ConjuntoTDA;
import tdas.DiccionarioMultipleTDA;
import tdas.PilaTDA;

public class EjerciciosUsoTdaparcialCuatro
{
	public static ConjuntoTDA aprobados(ConjuntoNotasTDA alumnos)
	{
		ConjuntoTDA aprobados = new ConjuntoDinamico();
		aprobados.inicializar();
		
		ConjuntoTDA legajos = alumnos.legajos();
			
		while (!legajos.estaVacio())
		{	
			int legajo = legajos.elegir();
			PilaTDA notaA = alumnos.notas(legajo);	
			
			int sumaNotas = 0;
			int cantidadNotas = 0;
			
			while (!notaA.estaVacia())
			{
				cantidadNotas++;
				sumaNotas += notaA.tope();
				notaA.desapilar();
			}
			
			if (sumaNotas / cantidadNotas >= 4)
			{
				aprobados.agregar(legajo);
			}
			
			legajos.sacar(legajo);
		}
		
		
		return aprobados;
	}
	
	public static ColaPrioridadTDA dos(DiccionarioMultipleTDA d)
	{
		ColaPrioridadTDA c = new ColaPrioridadDinamica();
		c.inicializar();
		
		ConjuntoTDA claves = d.claves();
		while (!claves.estaVacio())
		{
			int clave = claves.elegir();
			ConjuntoTDA valores = d.recuperar(clave);
			
			int cantidadValores = 0;
			while (!valores.estaVacio())
			{
				cantidadValores++;
				valores.sacar(valores.elegir());
			}
			
			c.acolarPrioridad(clave,cantidadValores);
			
			claves.sacar(clave);
		}
		
		
		return c;
	}
	
	public static boolean existeInversa(ColaPrioridadTDA c1,ColaPrioridadTDA c2)
	{
		boolean existe = true;
		
		ColaPrioridadTDA aux = new ColaPrioridadDinamica();
		aux.inicializar();
		
		while (existe && !c1.estaVacia())
		{
			boolean inverso = false;
			
			while (!inverso && !c2.estaVacia())
			{
				if (c1.primero() == c2.prioridad() &&
					c1.prioridad() == c2.primero())
				{
					inverso = true;
				}
				aux.acolarPrioridad(c2.primero(),c2.prioridad());
				c2.desacolar();
			}
			
			/*
			while (!c2.estaVacia())
			{
				aux.acolarPrioridad(c2.primero(),c2.prioridad());
				c2.desacolar();
			}
			*/
			
			while (!aux.estaVacia())
			{
				c2.acolarPrioridad(aux.primero(),aux.prioridad());
				aux.desacolar();
			}
			
			aux.inicializar();
			existe = inverso;
			c1.desacolar();
		}
		
		
		return existe;
	}
}
