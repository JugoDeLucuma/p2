package algoritmos;

import implementaciones.DiccionarioMultipleDinamico;
import tdas.ConjuntoTDA;
import tdas.DiccionarioMultipleTDA;

public class EjerciciosDiccionario {

	public static DiccionarioMultipleTDA todasLasClavesTodosLosElementos(DiccionarioMultipleTDA d1,
			DiccionarioMultipleTDA d2) 
	{
		DiccionarioMultipleTDA d = new DiccionarioMultipleDinamico();
		d.inicializar();
		
		ConjuntoTDA claves1 = d1.claves();
		ConjuntoTDA claves2 = d2.claves();
		
		while (!claves1.estaVacio() && !claves2.estaVacio())
		{
			int clave1 = claves1.elegir();
			int clave2 = claves2.elegir();
			
			ConjuntoTDA valores1 = d1.recuperar(clave1);
			ConjuntoTDA valores2 = d2.recuperar(clave2);
						
			int valor1 = 0;
			int valor2 = 0;
			
			while (!valores1.estaVacio() &&
				   !valores2.estaVacio())
			{
				valor1 = valores1.elegir();
				valor2 = valores2.elegir();
				
				d.agregar(clave1, valor1);
				d.agregar(clave2, valor2);
				
				valores1.sacar(valor1);
				valores2.sacar(valor2);
			}
			
			while (!valores1.estaVacio())
			{
				int valor = valores1.elegir();
				
				d.agregar(clave1, valor);
				valores1.sacar(valor);
			}
			
			while (!valores2.estaVacio())
			{
				int valor = valores2.elegir();
				
				d.agregar(clave2, valor);
				valores2.sacar(valor);
			}
						
			claves1.sacar(clave1);
			claves2.sacar(clave2);
		}
		
		while (!claves1.estaVacio())
		{
			int clave1 = claves1.elegir();
			ConjuntoTDA valores1 = d1.recuperar(clave1);
			
			while (!valores1.estaVacio())
			{
				int valor1 = valores1.elegir();
				d.agregar(clave1, valor1);
				valores1.sacar(valor1);
			}
			
			claves1.sacar(clave1);
		}
		
		while (!claves2.estaVacio())
		{
			int clave2 = claves2.elegir();
			ConjuntoTDA valores2 = d2.recuperar(clave2);
			
			while (!valores2.estaVacio())
			{
				int valor2 = valores2.elegir();
				d.agregar(clave2, valor2);
				valores2.sacar(valor2);
			}
			
			claves2.sacar(clave2);
		}
		
		return d;
	}

	public static DiccionarioMultipleTDA soloClavesCoincidentesTodosLosElementos(DiccionarioMultipleTDA d1,
			DiccionarioMultipleTDA d2)
	{
		DiccionarioMultipleTDA d = new DiccionarioMultipleDinamico();
		d.inicializar();
		
		ConjuntoTDA claves1 = d1.claves();
		ConjuntoTDA claves2 = d2.claves();
		
		while (!claves1.estaVacio() && !claves2.estaVacio())
		{
			int clave = claves1.elegir();
			
			if (claves2.pertenece(clave))
			{
				ConjuntoTDA valores1 = d1.recuperar(clave);
				ConjuntoTDA valores2 = d2.recuperar(clave);
				
				while (!valores1.estaVacio() && !valores2.estaVacio())
				{
					int valor1 = valores1.elegir();
					int valor2 = valores2.elegir();
					
					d.agregar(clave, valor1);
					d.agregar(clave, valor2);
					
					valores1.sacar(valor1);
					valores2.sacar(valor2);
				}
				
				while (!valores1.estaVacio())
				{
					int valor = valores1.elegir();
					
					d.agregar(clave, valor);
					valores1.sacar(valor);
				}
				
				while (!valores2.estaVacio())
				{
					int valor = valores2.elegir();
					
					d.agregar(clave, valor);
					valores2.sacar(valor);
				}
			}
			
			claves1.sacar(clave);
			claves2.sacar(clave);
		}
		
		return d;
	}

}
