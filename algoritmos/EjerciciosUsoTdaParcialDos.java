package algoritmos;

import implementaciones.ColaDinamica;
import implementaciones.ColaPrioridadDinamica;
import tdas.ABBTDA;
import tdas.ColaPrioridadTDA;
import tdas.ColaTDA;
import tdas.ConjuntoTDA;

public class EjerciciosUsoTdaParcialDos
{
	public static ColaPrioridadTDA a(ColaTDA cola,ConjuntoTDA c)
	{
		ColaPrioridadTDA cp = new ColaPrioridadDinamica();
		cp.inicializar();
		
		while (!cola.estaVacia())
		{
			int elemento = cola.primero();
			cola.desacolar();
			
			if (c.pertenece(elemento))
			{
				cp.acolarPrioridad(elemento, cantidadVeces(cola,elemento));
			}
		}
		
		return cp;
	}
	
	private static int cantidadVeces(ColaTDA cola, int elemento)
	{
		ColaTDA aux = new ColaDinamica();
		aux.inicializar();
		
		int cantidad = 1;
		
		while (!cola.estaVacia())
		{
			if (cola.primero() == elemento)
			{
				cantidad++;
			}
			else
			{
				aux.acolar(cola.primero());
			}
			
			cola.desacolar();
		}
		
		while (!aux.estaVacia())
		{
			cola.acolar(aux.primero());
			aux.desacolar();
		}
		
		return cantidad;
	}

	public static int sumaElementosProfundidadN(ABBTDA a,int n)
	{
		return sumaProf(a,n,0);
	}
	
	private static int sumaProf(ABBTDA a, int profBuscada,
			                    int profActual)
	{
		if (a.estaVacio())
		{
			return 0;
		}
		else
		{
			if (profBuscada == profActual)
			{
				return a.raiz();
			}
			else
			{
				return sumaProf(a.hijoIzquierdo(),
						        profBuscada, profActual+1)+
					   sumaProf(a.hijoDerecho(), profBuscada,
							   profActual+1);	
			}
		}
		
	}

	public static boolean sumaValoresNivelesIgual(ABBTDA a,
			                                      int n1,
			                                      int n2)
	{
		return sumaProf(a, n1, 0) == sumaProf(a,
				                              n2,
				                              0);
	}
}
