package tdas;

public interface PilaTopeVariableTDA
{
	/**
	 * inicializa la estructura
	 * PRE:
	 * POS:
	 */
	public void inicializar();
	
	/**
	 * indica si la pila esta vacia (true) o no (false)
	 * PRE: inicializada
	 * POS: 
	 */
	public boolean estaVacia();
	
	/**
	 * devuelve los n valores de la pila en un ConjuntoTDA
	 * si la cantidad de valores de la pila fuese menor
	 * a n, devuelve esa cantidad
	 * PRE: inicializada
	 * POS:
	 */
	public ConjuntoTDA tope(int n);
	
	/**
	 * elimina los n valores de la pila. Si hubiera menos
	 * de n valores, elimina todos los valores
	 * PRE: inicializada
	 * POS: la estructura tiene n valores menos
	 */
	public void desacolar(int n);
	
	/**
	 * agrega el elemento a la pila
	 * PRE: inicializada
	 * POS: la estructura tiene un elemento mas
	 */
	public void apilar(int n);
}
