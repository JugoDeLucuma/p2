package tdas;

public interface ConjuntoTDA 
{
	/**
	 * inicializa la estructura
	 * PRE:
	 * POS:
	 */
	public void inicializar();
	
	/**
	 * agrega elemento
	 * PRE: inicializada
	 * POS: la estructura tiene un elemento mas
	 *      si este no existiera, si existe no hace nada
	 */
	public void agregar(int elemento);
	
	/**
	 * saca el elemento deseado
	 * PRE: inicializada
	 * POS: si existe, la estructura queda con un elemento
	 *      menos
	 */
	public void sacar(int elemento);
	
	/**
	 * devuelve un elemento aleatorio
	 * PRE: inicializada y no vacio
	 * POS:
	 */
	public int elegir();
	
	/**
	 * indica si la estructura esta vacia
	 * PRE: inicializada
	 * POS:
	 */
	public boolean estaVacio();
	
	/**
	 * indica si el elemento pertenece al conjunto
	 * PRE: inicializada
	 * POS:
	 */
	public boolean pertenece(int elemento);
}
