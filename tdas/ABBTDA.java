package tdas;

public interface ABBTDA
{
	/**
	 * inicializa la estructura
	 * PRE:-
	 * POS:-
	 */
	public void inicializar();
	
	
	/**
	 * Devuelve la raiz
	 * PRE: inicializado y no vacio
	 * POS: -
	 * @return int
	 */
	public int raiz();
	
	
	/**
	 * indice si el arbol esta vacio
	 * PRE: inicializado
	 * POS: -
	 * @return boolean
	 */
	public boolean estaVacio();
	
	/**
	 * devuelve el hijo izquierdo
	 * PRE: inicializado y no vacio
	 * POS: -
	 * @return ABBTDA
	 */
	public ABBTDA hijoIzquierdo();
	
	/**
	 * devuelve el hijo derecho
	 * PRE: inicializado y no vacio
	 * POS: -
	 * @return ABBTDA
	 */
	public ABBTDA hijoDerecho();
	
	/**
	 * agrega un elemento al arbol
	 * PRE: inicializado
	 * POS: la estructura tiene un elemento mas
	 * @param elemento
	 */
	public void agregar(int elemento);

	/**
	 * elimina el elemento
	 * PRE: inicializado
	 * POS: la estructura tiene un elemento menos
	 * @param elemento
	 */
	public void eliminar(int elemento);
}
