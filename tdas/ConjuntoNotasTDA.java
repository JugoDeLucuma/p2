package tdas;

public interface ConjuntoNotasTDA
{
	/**
	 * inicializa la estructura
	 * PRE:
	 * POS:
	 */
	public void inicializar();
	
	/**
	 * agrega una nota al legajo. Si el legajo no existe lo
	 * crea. Si ya hay 4 notas, no hace nada
	 * PRE: inicializado
	 * POS: Si el legajo no existe, agrega un legajo y 
	 * nota mas. Si existe, agrega nota. Si las notas ya son 4
	 * no hace nada
	 */
	public void agregar(int legajo,int nota);
	
	/**
	 * elimina el legajo y sus notas
	 * PRE: inicializada
	 * POS: la estructura tiene un elemento menos
	 */
	public void eliminar(int legajo);
	
	/**
	 * elimina la nota del legajo
	 * PRE: inicializada, el legajo debe exitir
	 * POS: el legajo tiene una nota menos, si no tuviera mas
	 * notas, se borra el legajo 
	 */
	public void eliminarNota(int legajo,int nota);
	
	/**
	 * PRE: inicializada, el legajo debe existir
	 * POS:
	 */
	public PilaTDA notas(int legajo);
	
	/**
	 * PRE: inicializado
	 * POS:
	 */
	public ConjuntoTDA legajos();
	
	/**
	 * PRE:inicializado
	 * POS:
	 */
	public boolean estaVacia();
}
