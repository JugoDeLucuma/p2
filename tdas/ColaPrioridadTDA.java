package tdas;

public interface ColaPrioridadTDA
{
	/**
	 * 
	 */
	public void inicializar();
	
	/**
	 * 
	 */
	public void acolarPrioridad(int elemento, int prioridad);
	
	/**
	 * 
	 */
	public void desacolar();
	
	/**
	 * 
	 */
	public int primero();
	
	/**
	 * 
	 */
	public int prioridad();
	
	/**
	 * 
	 */
	public boolean estaVacia();
}
