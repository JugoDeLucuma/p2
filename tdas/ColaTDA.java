package tdas;

public interface ColaTDA 
{
	/**
	 * inicializa la cola
	 * PRE:
	 * POS: cola inicializada
	 */
	public void inicializar();
	
	/**
	 * agrega elemento a la cola
	 * PRE: inicializada
	 * POS: el elemento es agregado al final de la cola
	 */
	public void acolar(int elemento);
	
	/**
	 * saca el primer elemento de la cola
	 * PRE: inicializada y no vacia
	 * POS: la tama�o de la cola disminuye en uno
	 */
	public void desacolar();
	
	/**
	 * obtiene el valor del primer elemento
	 * PRE: inicializada y no vacia
	 * POS:
	 */
	public int primero();
	
	/**
	 * indica si la cola esta vacia
	 * PRE:inicializada
	 * POS:
	 */
	public boolean estaVacia();
}
