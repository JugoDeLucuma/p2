package tdas;

public interface DiccionarioSimpleTDA
{
	/**
	 * 
	 */
	public void inicializar();
	
	/**
	 * Pre:inicializado
	 * Post: agrega el par clave valor. Si la clave existe sobrescribe el valor
	 */
	public void agregar(int clave, int valor);
	
	/**
	 * Pre: inicializado
	 * Post: retorna el conjunto de claves
	 */
	public ConjuntoTDA claves();
	
	/**
	 * Pre: inicializado. La clave debe existir en el diccionario
	 * Post: retorna el valor asociado a la clave
	 */
	public int recuperar(int clave);
	
	/**
	 * Pre: inicializado.
	 * Post: elimina el par clave valor. Si la clave no existe no se hace nada
	 */
	public void eliminar(int clave);
}
