package tdas;

public interface PilaTDA
{
	/**
	 * inicializa la pila
	 * PRE:
	 * POS: pila inicializada
	 */
	public void inicializar();
	
	/**
	 * apila el elemento
	 * PRE: inicializada
	 * POS: la pila tiene un elemento m�s
	 */
	public void apilar(int elemento);
	
	/**
	 * desapila el ultimo elemento ingresado
	 * PRE: inicializada y no vacia
	 * POS: la pila tiene un elemento menos
	 */
	public void desapilar();
	
	/**
	 * devuelve el valor del ultimo valor ingresado
	 * PRE: inicializada y no vacio
	 * POS:
	 */
	public int tope();
	
	/**
	 * indica si la pila esta vacia
	 * PRE: inicializada
	 * POS:
	 */
	public boolean estaVacia();
}
