package tdas;

public interface DiccionarioMultipleTDA 
{
	/**
	 * 
	 */
	public void inicializar();
	
	/**
	 * 
	 */
	public void agregar(int clave, int valor);
	
	/**
	 * 
	 */
	public ConjuntoTDA claves();
	
	/**
	 * 
	 */
	public ConjuntoTDA recuperar(int clave);
	
	/**
	 * 
	 */
	public void eliminar(int clave);
	
	/**
	 * 
	 */
	public void eliminarValor(int clave, int valor);
}
