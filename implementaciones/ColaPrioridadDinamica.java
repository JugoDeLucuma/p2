package implementaciones;

import tdas.ColaPrioridadTDA;

public class ColaPrioridadDinamica implements ColaPrioridadTDA {

	class Nodo
	{
		int valor;
		int prioridad;
		Nodo siguiente;
	}
	
	private Nodo primero;
	
	@Override
	public void inicializar() 
	{
		primero = null;
	}

	@Override
	public void acolarPrioridad(int elemento, int prioridad)
	{
		Nodo nuevo = new Nodo();
		nuevo.valor = elemento;
		nuevo.prioridad = prioridad;
		
		if (primero == null ||
			prioridad > primero.prioridad)
		{
			nuevo.siguiente = primero;
			primero = nuevo;
		}
		else
		{
			Nodo anterior = primero;
			Nodo actual = primero.siguiente;
			
			while (actual != null &&
				   actual.prioridad > prioridad)
			{
				anterior = actual;
				actual = actual.siguiente;
			}
			
			anterior.siguiente = nuevo;
			nuevo.siguiente = actual;
		}
	}

	@Override
	public void desacolar()
	{
		primero = primero.siguiente;
	}

	@Override
	public int primero() 
	{
		return primero.valor;
	}

	@Override
	public int prioridad()
	{
		return primero.prioridad;
	}

	@Override
	public boolean estaVacia()
	{
		return primero == null;
	}

}
