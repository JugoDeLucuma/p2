package implementaciones;

import tdas.ColaTDA;

public class ColaDinamica implements ColaTDA
{

	class Nodo
	{
		int valor;
		Nodo siguiente;
	}
	
	private Nodo primero;
	private Nodo ultimo;
	
	@Override
	public void inicializar()
	{
		primero = null;
		ultimo = null;

	}

	@Override
	public void acolar(int elemento)
	{
		Nodo nuevo = new Nodo();
		nuevo.valor = elemento;
		nuevo.siguiente = null;
		
		if (ultimo != null)
		{
			ultimo.siguiente = nuevo;
		}
		
		ultimo = nuevo;
		
		if (primero == null)
		{
			primero = ultimo;
		}
	}

	@Override
	public void desacolar()
	{
		primero = primero.siguiente;
		
		if (primero == null)
		{
			ultimo = null;
		}
	}

	@Override
	public int primero()
	{
		return primero.valor;
	}

	@Override
	public boolean estaVacia()
	{
		return ultimo == null;
	}

}
