package implementaciones;

import tdas.ConjuntoTDA;
import tdas.PilaTopeVariableTDA;

public class PilaTopeVariable implements PilaTopeVariableTDA {

	class Nodo
	{
		int valor;
		Nodo siguiente;
	}
	
	private Nodo tope;
	
	@Override
	public void inicializar() 
	{
		tope = null;
	}

	@Override
	public boolean estaVacia()
	{
		return tope == null;
	}

	@Override
	public ConjuntoTDA tope(int n)
	{
		ConjuntoTDA c = new ConjuntoDinamico();
		c.inicializar();
		
		int cantidad = 0;
		Nodo aux = tope;
		
		while (aux != null && cantidad < n)
		{
			c.agregar(aux.valor);
			aux = aux.siguiente;
			cantidad++;
		}
		
		return c;
	}

	@Override
	public void desacolar(int n)
	{
		while (tope != null && n > 0)
		{
			tope = tope.siguiente;
			n--;
		}
	}

	@Override
	public void apilar(int n)
	{
		Nodo nuevo = new Nodo();
		nuevo.valor = n;
		nuevo.siguiente = tope;
		tope = nuevo;
	}

}
