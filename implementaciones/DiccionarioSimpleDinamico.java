package implementaciones;

import tdas.ConjuntoTDA;
import tdas.DiccionarioSimpleTDA;

public class DiccionarioSimpleDinamico implements DiccionarioSimpleTDA {

	class Nodo
	{
		int clave;
		int valor;
		Nodo siguiente;
	}
	
	private Nodo primero;
	
	@Override
	public void inicializar()
	{
		primero = null;
	}

	@Override
	public void agregar(int clave, int valor)
	{
		Nodo nodo = clave2Nodo(clave);
		
		if (nodo == null)
		{
			nodo = new Nodo();
			nodo.clave = clave;
			nodo.siguiente = primero;
			primero = nodo;
		}
		
		nodo.valor = valor;
	}

	private Nodo clave2Nodo(int clave) 
	{
		boolean encontrado = false;
		Nodo iterador = primero;
		
		while (!encontrado &&
			   iterador != null)
		{
			if (iterador.clave == clave)
			{
				encontrado = true;
			}
			else
			{
				iterador = iterador.siguiente;
			}
		}
		
		return iterador;
	}

	@Override
	public ConjuntoTDA claves()
	{
		ConjuntoTDA claves = new ConjuntoDinamico();
		claves.inicializar();
		
		Nodo iterador = primero;
		
		while (iterador != null)
		{
			claves.agregar(iterador.clave);
			iterador = iterador.siguiente;
		}
		
		return claves;
	}

	@Override
	public int recuperar(int clave)
	{
		Nodo nodo = clave2Nodo(clave);
		return nodo.valor;
	}

	@Override
	public void eliminar(int clave)
	{
		if (primero != null)
		{
			if (primero.clave == clave)
			{
				primero = primero.siguiente;
			}
			else
			{
				Nodo anterior = primero;
				Nodo actual = primero.siguiente;
				
				while (actual != null &&
					   actual.clave != clave)
				{
					anterior = actual;
					actual = actual.siguiente;
				}
				
				if (actual != null)
				{
					anterior.siguiente = actual.siguiente;
				}
			}
		}
	}

}
