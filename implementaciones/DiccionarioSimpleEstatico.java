package implementaciones;

import tdas.ConjuntoTDA;
import tdas.DiccionarioSimpleTDA;

public class DiccionarioSimpleEstatico implements DiccionarioSimpleTDA {

	class ParClaveValor
	{
		int clave;
		int valor;
	}
	
	private int cantidad;
	private ParClaveValor [] dic;
	
	@Override
	public void inicializar()
	{
		cantidad = 0;
		dic = new ParClaveValor[100];
	}

	@Override
	public void agregar(int clave, int valor)
	{
		ParClaveValor par = buscarPar(clave);
		
		if (par == null)
		{
			par = new ParClaveValor();
			par.clave = clave;
			dic[cantidad] = par;
			cantidad++;
		}
		
		par.valor = valor;
	}

	@Override
	public ConjuntoTDA claves() 
	{
		ConjuntoTDA claves = new ConjuntoEstatico();
		claves.inicializar();
		
		for (int iterador = 0;iterador < cantidad;++iterador)
		{
			claves.agregar(dic[iterador].clave);
		}
		
		return claves;
	}

	@Override
	public int recuperar(int clave) 
	{
		ParClaveValor par = buscarPar(clave);
		return par.valor;
	}

	@Override
	public void eliminar(int clave)
	{
		int indiceDeLaClave = claveToIndice(clave);
		if (indiceDeLaClave < cantidad)
		{
			dic[indiceDeLaClave] = dic[cantidad -1];
			cantidad--;
		}
	}
	
	private ParClaveValor buscarPar(int clave)
	{
		ParClaveValor aux = null;
				
		int iterador = 0;
		
		while (aux == null && iterador < cantidad)
		{
			if (dic[iterador].clave == clave)
			{
				aux = dic[iterador];
			}
			
			iterador++;
		}
		
		return aux;
	}
	
	
	private int claveToIndice(int clave)
	{
		int indice = 0;
		boolean claveEncontrada = false;
		
		while (!claveEncontrada && indice < cantidad)
		{
			if (clave == dic[indice].clave)
			{	
				claveEncontrada = true;
			}
			else
			{
				indice++;
			}
		}
		
		return indice;
	}
}
