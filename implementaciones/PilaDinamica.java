package implementaciones;

import tdas.PilaTDA;

public class PilaDinamica implements PilaTDA {

	class Nodo
	{
		int valor;
		Nodo sig;
	}
	
	private Nodo tope;
	
	@Override
	public void inicializar()
	{
		tope = null;

	}

	@Override
	public void apilar(int elemento) 
	{
		Nodo nuevo = new Nodo();
		nuevo.valor = elemento;
		nuevo.sig = tope;
		tope = nuevo;

	}

	@Override
	public void desapilar()
	{
		tope = tope.sig;
	}

	@Override
	public int tope()
	{
		return tope.valor;
	}

	@Override
	public boolean estaVacia()
	{
		return tope == null;
	}

}
