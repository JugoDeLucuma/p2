package implementaciones;

import tdas.ColaPrioridadTDA;

public class ColaPrioridadEstatica implements ColaPrioridadTDA
{
	private class Elemento
	{
		int valor;
		int prioridad;
	}
	
	private int cantidad;
	private Elemento[] cola;
	
	@Override
	public void inicializar()
	{
		cantidad = 0;
		cola = new Elemento[100];
	}

	@Override
	public void acolarPrioridad(int elemento, int prioridad)
	{
		int indice = cantidad;
		
		for (;indice > 0 && cola[indice-1].prioridad >= prioridad;indice--)
		{
			cola[indice] = cola[indice-1];
		}
		
		cola[indice] = new Elemento();
		cola[indice].valor = elemento;
		cola[indice].prioridad = prioridad;
		cantidad++;
	}

	@Override
	public void desacolar()
	{
		cola[cantidad-1] = null;
		cantidad--;
	}

	@Override
	public int primero()
	{
		return cola[cantidad-1].valor;
	}

	@Override
	public int prioridad()
	{
		return cola[cantidad-1].prioridad;
	}

	@Override
	public boolean estaVacia() 
	{
		return cantidad == 0;
	}

}
