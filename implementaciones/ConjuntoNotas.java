package implementaciones;

import tdas.ConjuntoNotasTDA;
import tdas.ConjuntoTDA;
import tdas.PilaTDA;

public class ConjuntoNotas implements ConjuntoNotasTDA
{
	class NodoNota
	{
		int nota;
		NodoNota siguiente;
	}
	
	class NodoLegajo
	{
		int legajo;
		NodoNota primeraNota;
		NodoLegajo siguiente;
	}
	
	private NodoLegajo primero;
	
	
	@Override
	public void inicializar() 
	{
		primero = null;
	}

	@Override
	public void agregar(int legajo, int nota)
	{
		NodoLegajo nodo = legajo2Nodo(legajo);
		
		if (nodo == null)
		{
			nodo = new NodoLegajo();
			nodo.legajo = legajo;
			nodo.siguiente = primero;
			primero = nodo;		
		}
		
		if (hayLugarParaMasNotas(nodo.primeraNota))
		{
			NodoNota nueva = new NodoNota();
			nueva.nota = nota;
			nueva.siguiente = nodo.primeraNota;
			nodo.primeraNota = nueva;
		}
	}

	private boolean hayLugarParaMasNotas(NodoNota primeraNota)
	{
		boolean hayLugar = true;
		NodoNota aux = primeraNota;
		int cantidadNotas = 0;
		
		while (hayLugar && aux != null)
		{
			cantidadNotas++;
			if (cantidadNotas == 4)
			{
				hayLugar = false;
			}
			aux = aux.siguiente;
		}
		
		return hayLugar;
	}

	@Override
	public void eliminar(int legajo)
	{
		if (primero != null)
		{
			if (primero.legajo == legajo)
			{
				primero = primero.siguiente;
			}
			else
			{
				NodoLegajo anterior = primero;
				NodoLegajo actual = primero.siguiente;
				
				while (actual != null && actual.legajo != legajo)
				{
					anterior = actual;
					actual = actual.siguiente;
				}
				
				if (actual != null)
				{
					anterior.siguiente = actual.siguiente;
				}
			}
		}

	}

	@Override
	public void eliminarNota(int legajo, int nota)
	{
		NodoLegajo buscado = legajo2Nodo(legajo);
		
		if (buscado != null)
		{
			if (buscado.primeraNota.nota == nota)
			{
				buscado.primeraNota = buscado.primeraNota.siguiente;
				
				if (buscado.primeraNota == null)
				{
					eliminar(legajo);
				}
			}
			else
			{
				NodoNota anterior = buscado.primeraNota;
				NodoNota actual = buscado.primeraNota.siguiente;
				
				while (actual != null && actual.nota != nota)
				{
					anterior = actual;
					actual = actual.siguiente;
				}
				
				if (actual != null)
				{
					anterior.siguiente = actual.siguiente;
				}
			}
		}

	}

	@Override
	public PilaTDA notas(int legajo)
	{
		PilaTDA notas = new PilaDinamica();
		notas.inicializar();
		
		NodoLegajo nodo = legajo2Nodo(legajo);
		
		if (nodo != null)
		{
			NodoNota iterador = nodo.primeraNota;
			
			while (iterador != null)
			{
				notas.apilar(iterador.nota);
				iterador = iterador.siguiente;
			}
		}
		
		return notas;
	}

	private NodoLegajo legajo2Nodo(int legajo)
	{
		NodoLegajo buscado = primero;
		boolean encontrado = false;
		
		while (!encontrado && buscado != null)
		{
			if (buscado.legajo == legajo)
			{
				encontrado = true;
			}
			else
			{
				buscado = buscado.siguiente;
			}
		}
		
		return buscado;
	}

	@Override
	public ConjuntoTDA legajos()
	{
		ConjuntoTDA legajos = new ConjuntoDinamico();
		legajos.inicializar();
		
		NodoLegajo iterador = primero;
		
		while (iterador != null)
		{
			legajos.agregar(iterador.legajo);
			iterador = iterador.siguiente;
		}
		
		return legajos;
	}

	@Override
	public boolean estaVacia()
	{
		return primero == null;
	}

}
