package implementaciones;

import tdas.PilaTDA;

public class PilaEstatica implements PilaTDA
{
	private int indice;
	private int [] pila;
	
	@Override
	public void inicializar()
	{
		indice = 0;
		pila = new int[100];
	}

	@Override
	public void apilar(int elemento) 
	{
		pila[indice] = elemento;
		indice++;
	}

	@Override
	public void desapilar()
	{
		indice--;
	}

	@Override
	public int tope()
	{
		return pila[indice-1];
	}

	@Override
	public boolean estaVacia()
	{
		return indice == 0;
	}

}
