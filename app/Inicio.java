package app;

import implementaciones.ABB;
import implementaciones.ColaDinamica;
import tdas.ABBTDA;
import tdas.ColaTDA;

public class Inicio 
{

	public static void main(String[] args)
	{
		ABBTDA a = new ABB();
		a.inicializar();
		
		a.agregar(10);
		a.agregar(15);
		a.agregar(5);
		a.agregar(1);
		a.agregar(7);
		a.agregar(11);
		a.agregar(25);
		a.agregar(6);
		a.agregar(20);
		a.agregar(27);
		a.agregar(18);
		
		Inicio inicio = new Inicio();
		System.out.println("preOrder");
		inicio.preOrder(a);
		System.out.println("----------");
		
		System.out.println("inOrder");
		inicio.inOrder(a);
		System.out.println("----------");
		
		System.out.println("postOrder");
		inicio.postOrder(a);
		System.out.println("----------");
		
		System.out.println("por niveles");
		inicio.porNiveles(a);
		System.out.println("----------");
		
	}
	
	private void porNiveles(ABBTDA arbol)
	{
		sumaProf(arbol,0,0);
		/*
		ColaTDA cola = new ColaDinamica();
		cola.inicializar();
		
		recorrer(arbol,cola);
		
		while (!cola.estaVacia())
		{
			System.out.println(cola.primero());
			cola.desacolar();
		}
		*/
		
	}

	private void recorrer(ABBTDA arbol, ColaTDA cola)
	{
		if (!arbol.estaVacio())
		{
			cola.acolar(arbol.raiz());
			
			if (!arbol.hijoIzquierdo().estaVacio())
			{
				recorrer(arbol.hijoIzquierdo(),cola);
			}
			
			if (!arbol.hijoDerecho().estaVacio())
			{
				recorrer(arbol.hijoDerecho(),cola);
			}
			
			
		}
		
	}

	private void sumaProf(ABBTDA a, int profBuscada,
			int profActual)
	{
		if (a.estaVacio())
		{
			
		}
		else
		{
			System.out.println(a.raiz());
			
			sumaProf(a.hijoIzquierdo(),profBuscada+1,
	                   profActual+1);
sumaProf(a.hijoDerecho(),profBuscada+1,
	                 profActual+1);	
			
			if (profBuscada == profActual)
			{
				
			}
			else
			{
				
			}
		}

	}

	/*
	 * visita primero los padres y luego
	 * cado hijo en el mismo orden, es decir
	 * raiz y luego cada subarbol en preorder
	 */
	private void preOrder(ABBTDA a)
	{
		if (!a.estaVacio())
		{
			System.out.println(a.raiz());
			preOrder(a.hijoIzquierdo());
			preOrder(a.hijoDerecho());
		}
	}
	
	/*
	 * visita inorder subarbol izquierdo,
	 * luego raiz y luego subarbol derecho.
	 * Este recorrido muestra los elementos
	 * ordenamos de menor a mayor
	 */
	private void inOrder(ABBTDA a)
	{
		if (!a.estaVacio())
		{
			inOrder(a.hijoIzquierdo());
			System.out.println(a.raiz());
			inOrder(a.hijoDerecho());
		}
	}
	
	/*
	 * visita primero los hijos en postorder
	 * y luego la raiz
	 */
	private void postOrder(ABBTDA a)
	{
		if (!a.estaVacio())
		{
			postOrder(a.hijoIzquierdo());
			postOrder(a.hijoDerecho());
			System.out.println(a.raiz());
			
		}
	}
	
}
