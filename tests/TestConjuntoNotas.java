package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import implementaciones.ConjuntoDinamico;
import implementaciones.ConjuntoNotas;
import tdas.ConjuntoNotasTDA;
import tdas.ConjuntoTDA;
import tdas.PilaTDA;

public class TestConjuntoNotas
{
	@Test
	public void testInicializar() 
	{
		ConjuntoNotasTDA c = new ConjuntoNotas();
		c.inicializar();
		
		assertTrue(c.estaVacia());
	}

	@Test
	public void testAgregarNotasALegajoInexistente()
	{
		ConjuntoNotasTDA c = new ConjuntoNotas();
		c.inicializar();
		
		c.agregar(1,1);
		assertFalse(c.estaVacia());
		assertEquals(1,c.notas(1).tope());
		
		c.agregar(1,5);
		c.agregar(1,6);
		c.agregar(1,7);
		
		
		ConjuntoTDA auxNotas = new ConjuntoDinamico();
		auxNotas.inicializar();
		auxNotas.agregar(1);
		auxNotas.agregar(5);
		auxNotas.agregar(6);
		auxNotas.agregar(7);
		
		PilaTDA notasP = c.notas(1);
		
		while (!notasP.estaVacia())
		{
			assertTrue(auxNotas.pertenece(notasP.tope()));
			notasP.desapilar();
		}
	}
	
	@Test
	public void testAgregarNotasADistintosLegajos()
	{
		ConjuntoNotasTDA c = new ConjuntoNotas();
		c.inicializar();
		
		c.agregar(1,1);
		c.agregar(1,5);
		
		c.agregar(2, 10);
		
		c.agregar(3, 5);
		c.agregar(3, 5);
		c.agregar(3, 5);
		
		ConjuntoTDA auxNotas = new ConjuntoDinamico();
		
		
		PilaTDA notasP = c.notas(1);
		auxNotas.inicializar();
		auxNotas.agregar(1);
		auxNotas.agregar(5);
		while (!notasP.estaVacia())
		{
			assertTrue(auxNotas.pertenece(notasP.tope()));
			notasP.desapilar();
		}
		
		
		notasP = c.notas(2);
		auxNotas.inicializar();
		auxNotas.agregar(10);
		while (!notasP.estaVacia())
		{
			assertTrue(auxNotas.pertenece(notasP.tope()));
			notasP.desapilar();
		}
		
		
		notasP = c.notas(3);
		auxNotas.inicializar();
		auxNotas.agregar(5);
		auxNotas.agregar(5);
		auxNotas.agregar(5);
		while (!notasP.estaVacia())
		{
			assertTrue(auxNotas.pertenece(notasP.tope()));
			notasP.desapilar();
		}
	}
	
	@Test
	public void testAgregarMasDeCuatroNotasALegajo()
	{
		ConjuntoNotasTDA c = new ConjuntoNotas();
		c.inicializar();
		
		c.agregar(1,1);
		c.agregar(1,5);
		c.agregar(1,6);
		c.agregar(1,7);
		
		c.agregar(1, 10);
		
		ConjuntoTDA auxNotas = new ConjuntoDinamico();
		auxNotas.inicializar();
		auxNotas.agregar(1);
		auxNotas.agregar(5);
		auxNotas.agregar(6);
		auxNotas.agregar(7);
		
		PilaTDA notasP = c.notas(1);
		int cantidadNotas = 0;
		
		while (!notasP.estaVacia())
		{
			assertTrue(auxNotas.pertenece(notasP.tope()));
			notasP.desapilar();
			cantidadNotas++;
		}
		
		assertEquals(4, cantidadNotas);
	}

	@Test
	public void testEliminar()
	{
		ConjuntoNotasTDA c = new ConjuntoNotas();
		c.inicializar();
		
		c.agregar(1,1);
		assertTrue(c.legajos().pertenece(1));
		c.eliminar(1);
		assertFalse(c.legajos().pertenece(1));
	}

	@Test
	public void testEliminarNota()
	{
		fail("Not yet implemented");
	}

	@Test
	public void testNotas()
	{
		fail("Not yet implemented");
	}

	@Test
	public void testLegajos() 
	{
		ConjuntoNotasTDA c = new ConjuntoNotas();
		c.inicializar();
		
		c.agregar(1,1);
		c.agregar(1,5);
		
		c.agregar(2, 10);
		
		c.agregar(3, 5);
		c.agregar(3, 5);
		c.agregar(3, 5);
		
		assertTrue(c.legajos().pertenece(1));
		assertTrue(c.legajos().pertenece(2));
		assertTrue(c.legajos().pertenece(3));
		assertFalse(c.legajos().pertenece(10));
	}

	@Test
	public void testEstaVacia()
	{		
		ConjuntoNotasTDA c = new ConjuntoNotas();
		c.inicializar();
		
		assertTrue(c.estaVacia());
		
		c.agregar(3, 5);
		
		assertFalse(c.estaVacia());
	}

}
