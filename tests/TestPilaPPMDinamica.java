package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import implementaciones.PilaPPMDinamica;
import tdas.PilaPPMTDA;

public class TestPilaPPMDinamica {

	@Test
	public void testInicializar()
	{
		PilaPPMTDA pila = new PilaPPMDinamica();
		pila.inicializar();
		
		assertTrue(pila.estaVacia());
	}

	@Test
	public void testEstaVacia()
	{
		PilaPPMTDA pila = new PilaPPMDinamica();
		pila.inicializar();
		
		assertTrue(pila.estaVacia());
		
		pila.apilar(1);
		
		assertFalse(pila.estaVacia());
	}

	@Test
	public void testTope() 
	{
		PilaPPMTDA pila = new PilaPPMDinamica();
		pila.inicializar();
				
		pila.apilar(1);
		assertEquals(1, pila.tope());
		pila.apilar(15);
		assertEquals(15, pila.tope());
	}

	@Test
	public void testApilar()
	{
		PilaPPMTDA pila = new PilaPPMDinamica();
		pila.inicializar();
		
		pila.apilar(1);
		assertEquals(1, pila.tope());
		pila.apilar(15);
		assertEquals(15, pila.tope());
		pila.apilar(10);
		assertEquals(15, pila.tope());
	}

	@Test
	public void testDesapilar() 
	{
		PilaPPMTDA pila = new PilaPPMDinamica();
		pila.inicializar();
		
		pila.apilar(1);
		assertFalse(pila.estaVacia());
		
		pila.desapilar();
		assertTrue(pila.estaVacia());
		
		pila.apilar(1);
		pila.apilar(2);
		pila.apilar(3);
		assertEquals(3, pila.tope());
		
		pila.desapilar();
		assertEquals(2, pila.tope());
		
		pila.desapilar();
		assertEquals(1, pila.tope());
	}

}
