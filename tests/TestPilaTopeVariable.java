package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import implementaciones.ConjuntoDinamico;
import implementaciones.PilaTopeVariable;
import tdas.ConjuntoTDA;
import tdas.PilaTopeVariableTDA;

public class TestPilaTopeVariable
{
	@Test
	public void testInicializar()
	{
		PilaTopeVariableTDA pila = new PilaTopeVariable();
		pila.inicializar();
		
		assertTrue(pila.estaVacia());
		
		pila.apilar(1);
		pila.apilar(4);
		pila.apilar(2);
		
		pila.inicializar();
		assertTrue(pila.estaVacia());
	}

	@Test
	public void testEstaVacia() 
	{
		PilaTopeVariableTDA pila = new PilaTopeVariable();
		pila.inicializar();
		
		assertTrue(pila.estaVacia());
		
		pila.apilar(1);
		pila.apilar(4);
		pila.apilar(2);
		
		assertFalse(pila.estaVacia());
	}

	@Test
	public void testTopeConNIgualCantidadElementos() 
	{
		PilaTopeVariableTDA pila = new PilaTopeVariable();
		pila.inicializar();
		
		pila.apilar(1);
		pila.apilar(4);
		pila.apilar(2);
		
		ConjuntoTDA c = new ConjuntoDinamico();
		c.inicializar();
		c.agregar(1);
		c.agregar(4);
		c.agregar(2);
		
		ConjuntoTDA valoresPila = pila.tope(3);
		
		assertTrue(sonIguales(c, valoresPila));
				
	}
	
	@Test
	public void testTopeConNMenorCantidadElementos()
	{
		PilaTopeVariableTDA pila = new PilaTopeVariable();
		pila.inicializar();
		
		pila.apilar(1);
		pila.apilar(4);
		pila.apilar(2);
		
		ConjuntoTDA c = new ConjuntoDinamico();
		c.inicializar();
		c.agregar(4);
		c.agregar(2);
		
		ConjuntoTDA valoresPila = pila.tope(2);
		
		assertTrue(sonIguales(c, valoresPila));
	}
	
	@Test
	public void testTopeConNMayorCantidadElementos()
	{
		PilaTopeVariableTDA pila = new PilaTopeVariable();
		pila.inicializar();
		
		pila.apilar(1);
		pila.apilar(4);
		pila.apilar(2);
		
		ConjuntoTDA c = new ConjuntoDinamico();
		c.inicializar();
		c.agregar(1);
		c.agregar(4);
		c.agregar(2);
		
		ConjuntoTDA valoresPila = pila.tope(4);
		
		assertTrue(sonIguales(c, valoresPila));
	}
	
	private boolean sonIguales(ConjuntoTDA c1, ConjuntoTDA c2)
	{
		boolean sonIguales = true;
		
		while (sonIguales &&
			   !c1.estaVacio() &&
			   !c2.estaVacio())
		{
			int elemento = c1.elegir();
			
			if (!c2.pertenece(elemento))
			{
				sonIguales = false;
			}
			
			c1.sacar(elemento);
			c2.sacar(elemento);
		}
		
		if (!c1.estaVacio() || !c2.estaVacio())
		{
			sonIguales = false;
		}
		
		return sonIguales;
	}
	
	@Test
	public void testDesacolarNIgualCantidadValores()
	{
		PilaTopeVariableTDA pila = new PilaTopeVariable();
		pila.inicializar();
		
		pila.apilar(1);
		pila.apilar(4);
		pila.apilar(2);
		
		assertFalse(pila.estaVacia());
		
		pila.desacolar(3);
		
		assertTrue(pila.estaVacia());
	}
	
	@Test
	public void testDesacolarNMenorCantidadValores()
	{
		PilaTopeVariableTDA pila = new PilaTopeVariable();
		pila.inicializar();
		
		pila.apilar(1);
		pila.apilar(4);
		pila.apilar(2);
		
		assertFalse(pila.estaVacia());
		
		pila.desacolar(2);
		
		assertFalse(pila.estaVacia());
		
		ConjuntoTDA c = new ConjuntoDinamico();
		c.inicializar();
		c.agregar(1);
		
		ConjuntoTDA valoresPila = pila.tope(1);
		
		assertTrue(sonIguales(c, valoresPila));
	}

	@Test
	public void testApilar()
	{
		PilaTopeVariableTDA pila = new PilaTopeVariable();
		pila.inicializar();
		
		assertTrue(pila.estaVacia());
		
		pila.apilar(1);
		pila.apilar(4);
		pila.apilar(2);
		
		assertFalse(pila.estaVacia());
	}

}
