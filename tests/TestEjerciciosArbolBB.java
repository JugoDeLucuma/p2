package tests;

import static org.junit.Assert.*;

import implementaciones.ABB;


import org.junit.Test;

import algoritmos.EjerciciosArbolBB;
import tdas.ABBTDA;
import tdas.ConjuntoTDA;

public class TestEjerciciosArbolBB 
{

	@Test
	public void testEsta()
	{
		ABBTDA arbol = new ABB();
		arbol.inicializar();
		
		arbol.agregar(20);
		arbol.agregar(10);
		arbol.agregar(35);
		arbol.agregar(1);
		arbol.agregar(18);
		arbol.agregar(14);
		arbol.agregar(12);
		arbol.agregar(15);
		arbol.agregar(26);
		arbol.agregar(40);
		arbol.agregar(23);
		arbol.agregar(30);
		
		assertTrue(EjerciciosArbolBB.esta(arbol,1));
		assertTrue(EjerciciosArbolBB.esta(arbol,12));
		assertTrue(EjerciciosArbolBB.esta(arbol,15));
		assertTrue(EjerciciosArbolBB.esta(arbol,18));
		assertTrue(EjerciciosArbolBB.esta(arbol,10));
		assertTrue(EjerciciosArbolBB.esta(arbol,14));
		assertTrue(EjerciciosArbolBB.esta(arbol,20));
		assertTrue(EjerciciosArbolBB.esta(arbol,23));
		assertTrue(EjerciciosArbolBB.esta(arbol,26));
		assertTrue(EjerciciosArbolBB.esta(arbol,30));
		assertTrue(EjerciciosArbolBB.esta(arbol,35));
		assertTrue(EjerciciosArbolBB.esta(arbol,40));
		
		assertFalse(EjerciciosArbolBB.esta(arbol,543));
		assertFalse(EjerciciosArbolBB.esta(arbol,344));
		assertFalse(EjerciciosArbolBB.esta(arbol,21));
		assertFalse(EjerciciosArbolBB.esta(arbol,2));
	}
	
	@Test
	public void testEstaConArbolVacio()
	{
		ABBTDA arbol = new ABB();
		arbol.inicializar();
		
		assertFalse(EjerciciosArbolBB.esta(arbol,543));
	}

	@Test
	public void testEsUnaHoja()
	{
		ABBTDA arbol = new ABB();
		arbol.inicializar();
		
		arbol.agregar(20);
		arbol.agregar(10);
		arbol.agregar(35);
		arbol.agregar(1);
		arbol.agregar(18);
		arbol.agregar(14);
		arbol.agregar(12);
		arbol.agregar(15);
		arbol.agregar(26);
		arbol.agregar(40);
		arbol.agregar(23);
		arbol.agregar(30);
		
		assertTrue(EjerciciosArbolBB.esUnaHoja(arbol,1));
		assertTrue(EjerciciosArbolBB.esUnaHoja(arbol,12));
		assertTrue(EjerciciosArbolBB.esUnaHoja(arbol,15));
		assertTrue(EjerciciosArbolBB.esUnaHoja(arbol,23));
		assertTrue(EjerciciosArbolBB.esUnaHoja(arbol,30));
		assertTrue(EjerciciosArbolBB.esUnaHoja(arbol,40));
		
		assertFalse(EjerciciosArbolBB.esUnaHoja(arbol,10));
		assertFalse(EjerciciosArbolBB.esUnaHoja(arbol,18));
		assertFalse(EjerciciosArbolBB.esUnaHoja(arbol,14));
		assertFalse(EjerciciosArbolBB.esUnaHoja(arbol,20));
		assertFalse(EjerciciosArbolBB.esUnaHoja(arbol,35));
		assertFalse(EjerciciosArbolBB.esUnaHoja(arbol,26));
	}

	@Test
	public void testProfundidad()
	{
		ABBTDA arbol = new ABB();
		arbol.inicializar();
		
		arbol.agregar(20);
		arbol.agregar(10);
		arbol.agregar(35);
		arbol.agregar(1);
		arbol.agregar(18);
		arbol.agregar(14);
		arbol.agregar(12);
		arbol.agregar(15);
		arbol.agregar(26);
		arbol.agregar(40);
		arbol.agregar(23);
		arbol.agregar(30);
		
		assertEquals(2,EjerciciosArbolBB.profundidad(arbol,1));
		assertEquals(2,EjerciciosArbolBB.profundidad(arbol,18));
		assertEquals(2,EjerciciosArbolBB.profundidad(arbol,26));
		assertEquals(2,EjerciciosArbolBB.profundidad(arbol,40));
		assertEquals(4,EjerciciosArbolBB.profundidad(arbol,12));
		assertEquals(4,EjerciciosArbolBB.profundidad(arbol,15));
		assertEquals(3,EjerciciosArbolBB.profundidad(arbol,14));
		assertEquals(3,EjerciciosArbolBB.profundidad(arbol,23));
		assertEquals(3,EjerciciosArbolBB.profundidad(arbol,30));
		assertEquals(1,EjerciciosArbolBB.profundidad(arbol,10));
		assertEquals(1,EjerciciosArbolBB.profundidad(arbol,35));
		assertEquals(0,EjerciciosArbolBB.profundidad(arbol,20));
	}

	@Test
	public void testMenorValor()
	{
		ABBTDA arbol = new ABB();
		arbol.inicializar();
		
		arbol.agregar(20);
		arbol.agregar(10);
		arbol.agregar(35);
		arbol.agregar(1);
		arbol.agregar(18);
		arbol.agregar(14);
		arbol.agregar(12);
		arbol.agregar(15);
		arbol.agregar(26);
		arbol.agregar(40);
		arbol.agregar(23);
		arbol.agregar(30);
		
		int menorValor = EjerciciosArbolBB.menorValor(arbol);
		
		assertEquals(1,menorValor);
	}

	@Test
	public void testCantidadElementos()
	{
		ABBTDA arbol = new ABB();
		arbol.inicializar();
		
		arbol.agregar(20);
		arbol.agregar(10);
		arbol.agregar(35);
		arbol.agregar(1);
		arbol.agregar(18);
		arbol.agregar(14);
		arbol.agregar(12);
		arbol.agregar(15);
		arbol.agregar(26);
		arbol.agregar(40);
		arbol.agregar(23);
		arbol.agregar(30);
		
		int cantidad = EjerciciosArbolBB.cantidadElementos(arbol);
		assertEquals(12,cantidad);
	}

	@Test
	public void testSumaElementos()
	{
		ABBTDA arbol = new ABB();
		arbol.inicializar();
		
		arbol.agregar(20);
		arbol.agregar(10);
		arbol.agregar(35);
		arbol.agregar(1);
		arbol.agregar(18);
		arbol.agregar(14);
		arbol.agregar(12);
		arbol.agregar(15);
		arbol.agregar(26);
		arbol.agregar(40);
		arbol.agregar(23);
		arbol.agregar(30);
		
		int suma = EjerciciosArbolBB.sumaElementos(arbol);
		assertEquals(244, suma);
	}

	@Test
	public void testCantidadHojas()
	{
		ABBTDA arbol = new ABB();
		arbol.inicializar();
		
		arbol.agregar(20);
		arbol.agregar(10);
		arbol.agregar(35);
		arbol.agregar(1);
		arbol.agregar(18);
		arbol.agregar(14);
		arbol.agregar(12);
		arbol.agregar(15);
		arbol.agregar(26);
		arbol.agregar(40);
		arbol.agregar(23);
		arbol.agregar(30);
		
		int cantidadHojas = EjerciciosArbolBB.cantidadHojas(arbol);
		assertEquals(6, cantidadHojas);
		
	}

	@Test
	public void testAltura() 
	{
		ABBTDA arbol = new ABB();
		arbol.inicializar();
		
		arbol.agregar(20);
		arbol.agregar(10);
		arbol.agregar(35);
		arbol.agregar(1);
		arbol.agregar(18);
		arbol.agregar(14);
		arbol.agregar(12);
		arbol.agregar(15);
		arbol.agregar(26);
		arbol.agregar(40);
		arbol.agregar(23);
		arbol.agregar(30);
		
		int altura = EjerciciosArbolBB.altura(arbol);
		assertEquals(4,altura);
	}

	@Test
	public void testTienenMismaForma() 
	{
		fail("Not yet implemented");
	}

	@Test
	public void testSonIguales()
	{
		fail("Not yet implemented");
	}

	@Test
	public void testCantidadElementosEnNivel()
	{
		fail("Not yet implemented");
	}
	
	@Test
	public void testNodosPares()
	{
		ABBTDA arbol = new ABB();
		arbol.inicializar();
		
		arbol.agregar(20);
		arbol.agregar(10);
		arbol.agregar(35);
		arbol.agregar(1);
		arbol.agregar(18);
		arbol.agregar(14);
		arbol.agregar(12);
		arbol.agregar(15);
		arbol.agregar(26);
		arbol.agregar(40);
		arbol.agregar(23);
		arbol.agregar(30);
		
		ConjuntoTDA nodosPares = EjerciciosArbolBB.nodosPares(arbol);
		
		assertTrue(nodosPares.pertenece(20));
		assertTrue(nodosPares.pertenece(10));
		assertTrue(nodosPares.pertenece(18));
		assertTrue(nodosPares.pertenece(14));
		assertTrue(nodosPares.pertenece(12));
		assertTrue(nodosPares.pertenece(26));
		assertTrue(nodosPares.pertenece(40));
		assertTrue(nodosPares.pertenece(30));
		
		assertFalse(nodosPares.pertenece(1));
		assertFalse(nodosPares.pertenece(35));
		assertFalse(nodosPares.pertenece(15));
		assertFalse(nodosPares.pertenece(23));
	}

	@Test
	public void testElementosMayoresQue()
	{
		ABBTDA arbol = new ABB();
		arbol.inicializar();
		
		arbol.agregar(20);
		arbol.agregar(10);
		arbol.agregar(35);
		arbol.agregar(1);
		arbol.agregar(18);
		arbol.agregar(14);
		arbol.agregar(12);
		arbol.agregar(15);
		arbol.agregar(26);
		arbol.agregar(40);
		arbol.agregar(23);
		arbol.agregar(30);
		
		int cantidadMay = EjerciciosArbolBB.cantidadElementosMayores(arbol,15);
		
		assertEquals(7,cantidadMay);
	}

}
