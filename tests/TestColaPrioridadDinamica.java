package tests;

import static org.junit.Assert.*;
import implementaciones.ColaPrioridadDinamica;

import org.junit.Test;

import tdas.ColaPrioridadTDA;

public class TestColaPrioridadDinamica
{

	@Test
	public void testInicializar()
	{
		ColaPrioridadTDA cola = new ColaPrioridadDinamica();
		cola.inicializar();
		assertTrue(cola.estaVacia());
	}

	@Test
	public void testAcolarPrioridad()
	{
		ColaPrioridadTDA cola = new ColaPrioridadDinamica();
		cola.inicializar();
		
		cola.acolarPrioridad(900,9);
		cola.acolarPrioridad(100,1);
		cola.acolarPrioridad(500,5);
		
		assertFalse(cola.estaVacia());
	}

	@Test
	public void testDesacolar()
	{
		ColaPrioridadTDA cola = new ColaPrioridadDinamica();
		cola.inicializar();
		
		cola.acolarPrioridad(900,9);
		cola.acolarPrioridad(100,1);
		cola.acolarPrioridad(500,5);
		
		while (!cola.estaVacia())
		{
			cola.desacolar();
		}
		
		assertTrue(cola.estaVacia());
	}

	@Test
	public void testPrimero()
	{
		ColaPrioridadTDA cola = new ColaPrioridadDinamica();
		cola.inicializar();
		
		cola.acolarPrioridad(100,1);
		cola.acolarPrioridad(900,9);
		cola.acolarPrioridad(500,5);
		
		assertEquals(900, cola.primero());
	}

	@Test
	public void testPrioridad()
	{
		ColaPrioridadTDA cola = new ColaPrioridadDinamica();
		cola.inicializar();
		
		cola.acolarPrioridad(100,1);
		cola.acolarPrioridad(900,9);
		cola.acolarPrioridad(500,5);
		
		assertEquals(9, cola.prioridad());
	}

	@Test
	public void testEstaVacia()
	{
		ColaPrioridadTDA cola = new ColaPrioridadDinamica();
		cola.inicializar();
		
		assertTrue(cola.estaVacia());
	}

}
