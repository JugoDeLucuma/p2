package tests;

import static org.junit.Assert.*;
import implementaciones.ColaDinamica;
import implementaciones.ConjuntoDinamico;
import implementaciones.PilaDinamica;
import tdas.ColaTDA;
import tdas.ConjuntoTDA;
import tdas.PilaTDA;

import org.junit.Test;

import algoritmos.EjercicioConjunto;



public class TestEjerciciosConjunto {

	@Test
	public void testInterseccion() {
		fail("Not yet implemented");
	}

	@Test
	public void testUnion() {
		fail("Not yet implemented");
	}

	@Test
	public void testDiferencia() {
		fail("Not yet implemented");
	}

	@Test
	public void testSonIguales()
	{
		ConjuntoTDA c1 = new ConjuntoDinamico();
		ConjuntoTDA c2 = new ConjuntoDinamico();
		
		c1.inicializar();
		c1.agregar(1);
		c1.agregar(1);
		c1.agregar(3);
		c1.agregar(7);
		
		c2.inicializar();
		c2.agregar(1);
		c2.agregar(1);
		c2.agregar(3);
		c2.agregar(7);
		
		assertTrue(EjercicioConjunto.sonIguales(c1, c2));
		
		c1.inicializar();
		c1.agregar(1);
		c1.agregar(1);
		c1.agregar(3);
		c1.agregar(7);
		
		c2.inicializar();
		c2.agregar(1);
		c2.agregar(1);
		c2.agregar(3);
		c2.agregar(7);
		c2.agregar(99);
		
		assertFalse(EjercicioConjunto.sonIguales(c1, c2));
	}

	@Test
	public void testCantidadElementos()
	{
		ConjuntoTDA c = new ConjuntoDinamico();
		c.inicializar();
		
		c.agregar(1);
		c.agregar(1);
		c.agregar(3);
		c.agregar(7);
		
		assertEquals(3, EjercicioConjunto.cantidadElementos(c));
		
	}

	@Test
	public void testGenerarAPartirDe()
	{
		ColaTDA cola = new ColaDinamica();
		cola.inicializar();
		cola.acolar(5);
		cola.acolar(8);
		cola.acolar(9);
		cola.acolar(2);
		cola.acolar(11);
		
		PilaTDA pila = new PilaDinamica();
		pila.inicializar();
		pila.apilar(1);
		pila.apilar(3);
		pila.apilar(7);
		pila.apilar(9);
		pila.apilar(5);
		
		ConjuntoTDA c = EjercicioConjunto.generarAPartirDe(pila, cola);
		assertFalse(c.estaVacio());
		assertTrue(c.pertenece(5));
		assertTrue(c.pertenece(9));
	}

	@Test
	public void testSonLosMismosElementos()
	{
		ColaTDA cola = new ColaDinamica();
		cola.inicializar();
		cola.acolar(5);
		cola.acolar(8);
		cola.acolar(9);
		cola.acolar(2);
		cola.acolar(11);
		PilaTDA pila = new PilaDinamica();
		pila.inicializar();
		pila.apilar(1);
		pila.apilar(3);
		pila.apilar(7);
		pila.apilar(9);
		pila.apilar(5);
		
		assertFalse(EjercicioConjunto.sonLosMismosElementos(pila, cola));
		
		cola.inicializar();
		cola.acolar(5);
		cola.acolar(8);
		cola.acolar(9);
		
		pila.inicializar();
		pila.apilar(5);
		pila.apilar(8);
		pila.apilar(5);
		pila.apilar(9);
		
		assertTrue(EjercicioConjunto.sonLosMismosElementos(pila, cola));
	}

}
