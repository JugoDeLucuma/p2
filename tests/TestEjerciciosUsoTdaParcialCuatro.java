package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import algoritmos.EjerciciosUsoTdaparcialCuatro;
import implementaciones.ColaPrioridadDinamica;
import implementaciones.ConjuntoNotas;
import implementaciones.DiccionarioMultipleDinamico;
import tdas.ColaPrioridadTDA;
import tdas.ConjuntoNotasTDA;
import tdas.ConjuntoTDA;
import tdas.DiccionarioMultipleTDA;

public class TestEjerciciosUsoTdaParcialCuatro {

	@Test
	public void testAprobados() 
	{
		ConjuntoNotasTDA c = new ConjuntoNotas();
		c.inicializar();
		
		c.agregar(1,5);
		c.agregar(1,6);
		c.agregar(1,7);
		c.agregar(1,10);
		
		c.agregar(2,4);
		c.agregar(2,4);
		c.agregar(2,4);
		c.agregar(2,4);
		
		c.agregar(3,3);
		c.agregar(3,2);
		c.agregar(3,3);
		c.agregar(3,2);
		
		ConjuntoTDA aprobados = EjerciciosUsoTdaparcialCuatro.aprobados(c);
		
		assertTrue(aprobados.pertenece(1));
		assertTrue(aprobados.pertenece(2));
		assertFalse(aprobados.pertenece(3));
	}

	@Test
	public void testDos() 
	{
		DiccionarioMultipleTDA d = new DiccionarioMultipleDinamico();
		d.inicializar();
		
		d.agregar(1, 1);
		d.agregar(1, 2);
		d.agregar(1, 3);
		d.agregar(1, 4);
		
		d.agregar(2, 1);
		d.agregar(2, 2);
		d.agregar(2, 3);
		
		d.agregar(3, 1);
		d.agregar(3, 2);
		
		d.agregar(4, 1);
		
		ColaPrioridadTDA c = EjerciciosUsoTdaparcialCuatro.dos(d);
		
		assertEquals(1, c.primero());
		assertEquals(4, c.prioridad());
		c.desacolar();
		
		assertEquals(2, c.primero());
		assertEquals(3, c.prioridad());
		c.desacolar();
		
		assertEquals(3, c.primero());
		assertEquals(2, c.prioridad());
		c.desacolar();
		
		assertEquals(4, c.primero());
		assertEquals(1, c.prioridad());
		c.desacolar();
	}

	@Test
	public void testExisteInversa() 
	{
		ColaPrioridadTDA c1 = new ColaPrioridadDinamica();
		c1.inicializar();
		c1.acolarPrioridad(1,10);
		c1.acolarPrioridad(2,9);
		c1.acolarPrioridad(3,8);
		c1.acolarPrioridad(4,7);
		
		ColaPrioridadTDA c2 = new ColaPrioridadDinamica();
		c2.inicializar();
		c2.acolarPrioridad(9,6);
		c2.acolarPrioridad(7,5);
		c2.acolarPrioridad(7,4);
		c2.acolarPrioridad(8,3);
		c2.acolarPrioridad(9,2);
		c2.acolarPrioridad(10,1);
		
		assertTrue(EjerciciosUsoTdaparcialCuatro.existeInversa(c1,c2));
	}
	
	@Test
	public void testExisteInversaChico() 
	{
		ColaPrioridadTDA c1 = new ColaPrioridadDinamica();
		c1.inicializar();
		c1.acolarPrioridad(1,10);
		c1.acolarPrioridad(2,9);
		
		
		ColaPrioridadTDA c2 = new ColaPrioridadDinamica();
		c2.inicializar();
		
		c2.acolarPrioridad(9,2);
		c2.acolarPrioridad(10,1);
		
		assertTrue(EjerciciosUsoTdaparcialCuatro.existeInversa(c1,c2));
	}


}
