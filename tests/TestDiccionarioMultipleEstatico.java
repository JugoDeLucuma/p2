package tests;

import static org.junit.Assert.*;
import implementaciones.DiccionarioMultipleEstatico;

import org.junit.Test;

import tdas.ConjuntoTDA;
import tdas.DiccionarioMultipleTDA;

public class TestDiccionarioMultipleEstatico {

	@Test
	public void testInicializar()
	{
		DiccionarioMultipleTDA diccionario = new DiccionarioMultipleEstatico();
		
		diccionario.inicializar();
		assertTrue(diccionario.claves().estaVacio());
		
		diccionario.agregar(5, 50);
		diccionario.inicializar();
		assertTrue(diccionario.claves().estaVacio());
	}

	@Test
	public void testAgregar()
	{
		DiccionarioMultipleTDA diccionario = new DiccionarioMultipleEstatico();
		diccionario.inicializar();
		
		diccionario.agregar(5, 50);
		assertFalse(diccionario.claves().estaVacio());
		assertTrue(diccionario.recuperar(5).pertenece(50));
		
		diccionario.agregar(5, 51);
		diccionario.agregar(6, 60);
		diccionario.agregar(6, 61);
		diccionario.agregar(6, 62);
		diccionario.agregar(7, 70);
		assertFalse(diccionario.claves().estaVacio());
		assertTrue(diccionario.recuperar(5).pertenece(50));
		assertTrue(diccionario.recuperar(5).pertenece(51));
		assertTrue(diccionario.recuperar(6).pertenece(60));
		assertTrue(diccionario.recuperar(6).pertenece(61));
		assertTrue(diccionario.recuperar(6).pertenece(62));
		assertTrue(diccionario.recuperar(7).pertenece(70));
		
		diccionario.agregar(8, 80);
		diccionario.agregar(8, 80);
		diccionario.agregar(8, 80);
		
		ConjuntoTDA conjuntoValores = diccionario.recuperar(8);
		int cantidadElementos = 0;
		
		while (!conjuntoValores.estaVacio())
		{
			conjuntoValores.sacar(conjuntoValores.elegir());
			cantidadElementos++;
		}
		
		assertEquals(1, cantidadElementos);
		
	}

	@Test
	public void testEliminar() 
	{
		DiccionarioMultipleTDA diccionario = new DiccionarioMultipleEstatico();
		diccionario.inicializar();
		
		assertTrue(diccionario.claves().estaVacio());
		diccionario.eliminar(3);
		assertTrue(diccionario.claves().estaVacio());
		
		diccionario.agregar(3,30);
		assertFalse(diccionario.claves().estaVacio());
		diccionario.eliminar(3);
		assertTrue(diccionario.claves().estaVacio());
		
		diccionario.agregar(5,50);
		diccionario.agregar(6,60);
		diccionario.agregar(7,70);
		
		diccionario.eliminar(3);
		diccionario.eliminar(5);
		diccionario.eliminar(7);
		
		int cantidadClaves = 0;
		ConjuntoTDA claves = diccionario.claves();
		
		while (!claves.estaVacio())
		{
			int clave = claves.elegir();
			
			assertTrue(claves.pertenece(clave));
			claves.sacar(clave);
			cantidadClaves++;
		}
		
		assertEquals(1,cantidadClaves);
	}

	@Test
	public void testEliminarValorClaveQueNoExista()
	{
		DiccionarioMultipleTDA diccionario1 = new DiccionarioMultipleEstatico();
		diccionario1.inicializar();
		
		diccionario1.agregar(1,10);
		diccionario1.agregar(1,11);
		diccionario1.agregar(1,12);
		diccionario1.agregar(2,20);
		
		diccionario1.eliminarValor(3,30);
		
		int cantidadClaves = 0;
		
		ConjuntoTDA claves = diccionario1.claves();
		assertFalse(claves.pertenece(3));
				
		while (!claves.estaVacio())
		{
			int clave = claves.elegir();
			
			assertTrue(claves.pertenece(clave));
			claves.sacar(clave);
			cantidadClaves++;
		}
		
		assertEquals(2,cantidadClaves);
	}
	
	@Test
	public void testEliminarValorValorQueNoExista()
	{
		DiccionarioMultipleTDA diccionario2 = new DiccionarioMultipleEstatico();
		diccionario2.inicializar();
		
		diccionario2.agregar(1,10);
		diccionario2.agregar(1,11);
		diccionario2.agregar(1,12);
		diccionario2.agregar(2,20);
		
		diccionario2.eliminarValor(1,25);
		
		int cantidadValores = 0;
		ConjuntoTDA valores = diccionario2.recuperar(1);
				
		while (!valores.estaVacio())
		{
			int valor = valores.elegir();
			
			assertTrue(valores.pertenece(valor));
			valores.sacar(valor);
			cantidadValores++;
		}
		
		assertEquals(3,cantidadValores);
	}
	
	@Test
	public void testEliminarValorQueNoSeaElUltimoDeLaClave()
	{
		DiccionarioMultipleTDA diccionario2 = new DiccionarioMultipleEstatico();
		diccionario2.inicializar();
		
		diccionario2.agregar(1,10);
		diccionario2.agregar(1,11);
		diccionario2.agregar(1,12);
		diccionario2.agregar(2,20);
		
		diccionario2.eliminarValor(1,10);
		
		int cantidadValores = 0;
		ConjuntoTDA valores2 = diccionario2.recuperar(1);
				
		while (!valores2.estaVacio())
		{
			int valor = valores2.elegir();
			
			assertTrue(valores2.pertenece(valor));
			valores2.sacar(valor);
			cantidadValores++;
		}
		
		assertEquals(2,cantidadValores);
		assertTrue(diccionario2.claves().pertenece(1));
	}
	
	@Test
	public void testEliminarValorQueSeaElUltimoDeLaClave()
	{
		DiccionarioMultipleTDA diccionario2 = new DiccionarioMultipleEstatico();
		diccionario2.inicializar();
		
		diccionario2.agregar(1,10);
		diccionario2.agregar(1,11);
		diccionario2.agregar(1,12);
		diccionario2.agregar(2,20);
		
		diccionario2.eliminarValor(2,20);
		assertFalse(diccionario2.claves().pertenece(2));
	}
	
	@Test
	public void testClaves()
	{
		DiccionarioMultipleTDA diccionario = new DiccionarioMultipleEstatico();
		diccionario.inicializar();
		
		assertTrue(diccionario.claves().estaVacio());
		
		diccionario.agregar(5,50);
		diccionario.agregar(6,60);
		diccionario.agregar(7,70);
		diccionario.agregar(7,71);
		
		assertFalse(diccionario.claves().estaVacio());
		
		int cantidadClaves = 0;
		ConjuntoTDA claves = diccionario.claves();
		
		assertFalse(claves.pertenece(567));
		
		while (!claves.estaVacio())
		{
			int clave = claves.elegir();
			
			assertTrue(claves.pertenece(clave));
			claves.sacar(clave);
			cantidadClaves++;
		}
		
		assertEquals(3,cantidadClaves);
	}

	@Test
	public void testRecuperar()
	{
		DiccionarioMultipleTDA diccionario = new DiccionarioMultipleEstatico();
		diccionario.inicializar();
		
		diccionario.agregar(5,50);
		diccionario.agregar(6,60);
		diccionario.agregar(7,70);
		diccionario.agregar(7,71);
		
		ConjuntoTDA conjuntoValores = diccionario.recuperar(8);
		int cantidadElementos = 0;
		
		while (!conjuntoValores.estaVacio())
		{
			conjuntoValores.sacar(conjuntoValores.elegir());
			cantidadElementos++;
		}
		
		assertEquals(0, cantidadElementos);
		
		cantidadElementos = 0;
		conjuntoValores.inicializar();
		
		conjuntoValores = diccionario.recuperar(7);
		
		while (!conjuntoValores.estaVacio())
		{
			conjuntoValores.sacar(conjuntoValores.elegir());
			cantidadElementos++;
		}
		
		assertEquals(2, cantidadElementos);
	}

}
