package tests;

import static org.junit.Assert.*;
import implementaciones.ConjuntoDinamico;

import org.junit.Test;

import tdas.ConjuntoTDA;

public class TestConjuntoDinamico
{

	@Test
	public void testInicializar()
	{
		ConjuntoTDA conjunto = new ConjuntoDinamico();
		conjunto.inicializar();
		
		assertTrue(conjunto.estaVacio());
	}

	@Test
	public void testAgregar()
	{
		ConjuntoTDA conjunto = new ConjuntoDinamico();
		conjunto.inicializar();
		
		conjunto.agregar(1);
		conjunto.agregar(1);
		conjunto.agregar(3);
		conjunto.agregar(5);
		
		assertFalse(conjunto.estaVacio());
		
		int cantidadElementos = 0;
		
		while (!conjunto.estaVacio())
		{
			conjunto.sacar(conjunto.elegir());
			cantidadElementos++;
		}
		
		assertEquals(3,cantidadElementos);
	}

	@Test
	public void testSacar()
	{
		ConjuntoTDA conjunto = new ConjuntoDinamico();
		conjunto.inicializar();
		
		conjunto.agregar(1);
		conjunto.sacar(1);
		conjunto.sacar(2);
		
		assertFalse(conjunto.pertenece(1));
		assertTrue(conjunto.estaVacio());
	}

	@Test
	public void testElegir()
	{
		ConjuntoTDA conjunto = new ConjuntoDinamico();
		conjunto.inicializar();
		
		conjunto.agregar(1);
		conjunto.agregar(2);
		conjunto.agregar(3);
		conjunto.agregar(5);
		
		assertNotNull(conjunto.elegir());
	}

	@Test
	public void testPertenece()
	{
		ConjuntoTDA conjunto = new ConjuntoDinamico();
		conjunto.inicializar();
		
		assertFalse(conjunto.pertenece(1));
		
		conjunto.agregar(1);
		assertTrue(conjunto.pertenece(1));
		assertFalse(conjunto.pertenece(65));
		
		conjunto.agregar(65);
		conjunto.agregar(23);
		
		assertTrue(conjunto.pertenece(23));
		assertTrue(conjunto.pertenece(1));
		assertTrue(conjunto.pertenece(23));
		assertTrue(conjunto.pertenece(65));
		assertFalse(conjunto.pertenece(45));
		assertTrue(conjunto.pertenece(65));
		assertFalse(conjunto.pertenece(345));
		assertFalse(conjunto.pertenece(3434));
	}

	@Test
	public void testEstaVacio()
	{
		ConjuntoTDA conjunto = new ConjuntoDinamico();
		
		conjunto.inicializar();
		assertTrue(conjunto.estaVacio());
		
		conjunto.agregar(1);
		conjunto.sacar(1);
		assertTrue(conjunto.estaVacio());
		
		conjunto.agregar(1);
		conjunto.inicializar();
		assertTrue(conjunto.estaVacio());
		
		conjunto.agregar(1);
		assertFalse(conjunto.estaVacio());
	}

}
