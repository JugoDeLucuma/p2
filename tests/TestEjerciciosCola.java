package tests;

import static org.junit.Assert.*;
import implementaciones.ColaDinamica;
import implementaciones.ColaEstatica;
import tdas.ColaTDA;
import tdas.ConjuntoTDA;

import org.junit.Test;

import algoritmos.EjerciciosCola;



public class TestEjerciciosCola
{

	@Test
	public void testPasarColaAOtra() 
	{
		ColaTDA original = new ColaDinamica();
		original.inicializar();
		
		original.acolar(3);
		original.acolar(34);
		original.acolar(23);
		original.acolar(6);
		original.acolar(8);
		
		ColaTDA otraCola = EjerciciosCola.pasarColaAOtra(original);
		
		assertEquals(3, otraCola.primero());
		otraCola.desacolar();
		assertEquals(34, otraCola.primero());
		otraCola.desacolar();
		assertEquals(23, otraCola.primero());
		otraCola.desacolar();
		assertEquals(6, otraCola.primero());
		otraCola.desacolar();
		assertEquals(8, otraCola.primero());
		otraCola.desacolar();
		
	}

	@Test
	public void testInvertirColaUsandoPilas()
	{
		ColaTDA original = new ColaDinamica();
		original.inicializar();
		
		original.acolar(3);
		original.acolar(34);
		original.acolar(23);
		original.acolar(6);
		original.acolar(8);
		
		ColaTDA otraCola = EjerciciosCola.invertirColaUsandoPilas(original);
		
		assertEquals(8, otraCola.primero());
		otraCola.desacolar();
		assertEquals(6, otraCola.primero());
		otraCola.desacolar();
		assertEquals(23, otraCola.primero());
		otraCola.desacolar();
		assertEquals(34, otraCola.primero());
		otraCola.desacolar();
		assertEquals(3, otraCola.primero());
		otraCola.desacolar();
	}

	@Test
	public void testDeterminarSiElFinalDeUnaColaEsIgualAlDeOtra()
	{
		ColaTDA cola1 = new ColaDinamica();
		ColaTDA cola2 = new ColaEstatica();
		
		cola1.inicializar();
		cola1.acolar(2);
		cola1.acolar(24);
		cola1.acolar(25);
		cola1.acolar(7);
		
		cola2.inicializar();
		cola2.acolar(54);
		cola2.acolar(23);
		cola2.acolar(45);
		
		assertFalse(EjerciciosCola.determinarSiElFinalDeUnaColaEsIgualAlDeOtra(cola1, cola2));
		
		cola1.inicializar();
		cola1.acolar(2);
		cola1.acolar(24);
		cola1.acolar(25);
		cola1.acolar(7);
		
		cola2.inicializar();
		cola2.acolar(54);
		cola2.acolar(23);
		cola2.acolar(7);
		
		assertTrue(EjerciciosCola.determinarSiElFinalDeUnaColaEsIgualAlDeOtra(cola1, cola2));
	}

	@Test
	public void testEsCapicua()
	{
		ColaTDA cola = new ColaDinamica();
		
		cola.inicializar();
		cola.acolar(1);
		cola.acolar(5);
		cola.acolar(7);
		cola.acolar(7);
		assertFalse(EjerciciosCola.esCapicua(cola));
		
		cola.inicializar();
		cola.acolar(1);
		cola.acolar(5);
		cola.acolar(1);
		assertTrue(EjerciciosCola.esCapicua(cola));
		
		cola.inicializar();
		cola.acolar(2);
		cola.acolar(3);
		cola.acolar(4);
		cola.acolar(5);
		cola.acolar(4);
		cola.acolar(3);
		cola.acolar(2);
		assertTrue(EjerciciosCola.esCapicua(cola));
	}	

	@Test
	public void testSonInversas()
	{
		ColaTDA cola1 = new ColaDinamica();
		ColaTDA cola2 = new ColaEstatica();
		
		cola1.inicializar();
		cola1.acolar(2);
		cola1.acolar(24);
		cola1.acolar(25);
		cola1.acolar(7);
		
		cola2.inicializar();
		cola2.acolar(54);
		cola2.acolar(23);
		cola2.acolar(45);
		
		assertFalse(EjerciciosCola.sonInversas(cola1, cola2));
		
		cola1.inicializar();
		cola1.acolar(2);
		cola1.acolar(24);
		cola1.acolar(25);
		cola1.acolar(7);
		
		cola2.inicializar();
		cola2.acolar(7);
		cola2.acolar(25);
		cola2.acolar(24);
		cola2.acolar(2);
		
		assertTrue(EjerciciosCola.sonInversas(cola1, cola2));
		
		cola1.inicializar();
		cola1.acolar(2);
		cola1.acolar(24);
		cola1.acolar(25);
		cola1.acolar(7);
		
		cola2.inicializar();
		cola2.acolar(7);
		cola2.acolar(25);
		cola2.acolar(24);
		cola2.acolar(2);
		cola2.acolar(24);
		
		assertFalse(EjerciciosCola.sonInversas(cola1, cola2));
	}

	@Test
	public void testSacarRepetidos()
	{
		ColaTDA cola = new ColaDinamica();
		
		cola.inicializar();
		cola.acolar(1);
		cola.acolar(5);
		cola.acolar(7);
		cola.acolar(7);
		
		EjerciciosCola.sacarRepetidos(cola);
		int cantidad = 0;
		
		while (!cola.estaVacia())
		{
			cantidad++;
			cola.desacolar();
		}
		
		assertEquals(3,cantidad);
	}

	@Test
	public void testGenerarConjuntoRepetidos()
	{
		ColaTDA cola = new ColaDinamica();
		
		cola.inicializar();
		cola.acolar(1);
		cola.acolar(5);
		cola.acolar(7);
		cola.acolar(7);
		cola.acolar(67);
		cola.acolar(5);
		cola.acolar(67);
		
		ConjuntoTDA repetidos = EjerciciosCola.generarConjuntoRepetidos(cola);
		
		assertFalse(repetidos.estaVacio());
		assertTrue(repetidos.pertenece(5));
		assertTrue(repetidos.pertenece(7));
		assertTrue(repetidos.pertenece(67));
		assertFalse(repetidos.pertenece(1));
		
	}

}
