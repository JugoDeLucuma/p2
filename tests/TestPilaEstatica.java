package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import implementaciones.PilaEstatica;
import tdas.PilaTDA;

public class TestPilaEstatica
{

	@Test
	public void testInicializarGeneraUnaPilaVacia()
	{
		PilaTDA pila = new PilaEstatica();
		pila.inicializar();
		
		assertTrue(pila.estaVacia());
		
		pila.apilar(1);
		pila.apilar(4);
		pila.apilar(2);
		
		pila.inicializar();
		assertTrue(pila.estaVacia());
	}
	
	@Test
	public void testQueSeaLifo()
	{
		PilaTDA pila = new PilaEstatica();
		pila.inicializar();
		
		pila.apilar(1);
		pila.apilar(4);
		pila.apilar(2);
		
		assertEquals(2, pila.tope());
		
		pila.desapilar();
		assertEquals(4,pila.tope());
		
		pila.desapilar();
		assertEquals(1,pila.tope());
	}
	
	@Test
	public void testApiloYDesapiloYQuedaVacia()
	{
		PilaTDA pila = new PilaEstatica();
		pila.inicializar();
		
		pila.apilar(1);
		assertFalse(pila.estaVacia());
		pila.desapilar();
		assertTrue(pila.estaVacia());
	}
	
	@Test
	public void testApiloYDesapiloLaMismaCantidadDeElementos()
	{
		PilaTDA pila = new PilaEstatica();
		pila.inicializar();
		
		pila.apilar(1);
		pila.apilar(4);
		pila.apilar(2);
		
		int cantidadDeElementos = 0;
		
		while (!pila.estaVacia())
		{
			cantidadDeElementos++;
			pila.desapilar();
		}
		
		assertEquals(3, cantidadDeElementos);
	}

}
