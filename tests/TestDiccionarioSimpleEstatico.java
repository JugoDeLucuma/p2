package tests;

import static org.junit.Assert.*;
import implementaciones.DiccionarioSimpleEstatico;

import org.junit.Test;

import tdas.ConjuntoTDA;
import tdas.DiccionarioSimpleTDA;

public class TestDiccionarioSimpleEstatico
{

	@Test
	public void testInicializar()
	{
		DiccionarioSimpleTDA diccionario = new DiccionarioSimpleEstatico();
		
		diccionario.inicializar();
		assertTrue(diccionario.claves().estaVacio());
		
		diccionario.agregar(5, 50);
		diccionario.inicializar();
		assertTrue(diccionario.claves().estaVacio());
	}

	@Test
	public void testAgregar()
	{
		DiccionarioSimpleTDA diccionario = new DiccionarioSimpleEstatico();
		diccionario.inicializar();
		
		diccionario.agregar(5,50);
		assertFalse(diccionario.claves().estaVacio());
		assertEquals(50,diccionario.recuperar(5));
		
		diccionario.agregar(1,10);
		diccionario.agregar(4,40);
		diccionario.agregar(3,30);
		assertFalse(diccionario.claves().estaVacio());
		assertEquals(30,diccionario.recuperar(3));
		assertEquals(40,diccionario.recuperar(4));
		assertEquals(10,diccionario.recuperar(1));
		assertEquals(50,diccionario.recuperar(5));
		
		diccionario.agregar(5,55);
		assertFalse(diccionario.claves().estaVacio());
		assertEquals(30,diccionario.recuperar(3));
		assertEquals(40,diccionario.recuperar(4));
		assertEquals(10,diccionario.recuperar(1));
		assertEquals(55,diccionario.recuperar(5));
	}

	@Test
	public void testEliminar()
	{
		DiccionarioSimpleTDA diccionario = new DiccionarioSimpleEstatico();
		diccionario.inicializar();
		
		assertTrue(diccionario.claves().estaVacio());
		diccionario.eliminar(3);
		assertTrue(diccionario.claves().estaVacio());
		
		diccionario.agregar(3,30);
		assertFalse(diccionario.claves().estaVacio());
		diccionario.eliminar(3);
		assertTrue(diccionario.claves().estaVacio());
		
		diccionario.agregar(5,50);
		diccionario.agregar(6,60);
		diccionario.agregar(7,70);
		
		diccionario.eliminar(3);
		diccionario.eliminar(5);
		diccionario.eliminar(7);
		
		int cantidadClaves = 0;
		ConjuntoTDA claves = diccionario.claves();
		
		while (!claves.estaVacio())
		{
			int clave = claves.elegir();
			
			assertTrue(claves.pertenece(clave));
			claves.sacar(clave);
			cantidadClaves++;
		}
		
		assertEquals(1,cantidadClaves);
	}

	@Test
	public void testRecuperar()
	{
		DiccionarioSimpleTDA diccionario = new DiccionarioSimpleEstatico();
		diccionario.inicializar();
		
		diccionario.agregar(1,10);
		diccionario.agregar(4,40);
		diccionario.agregar(3,30);
		
		assertEquals(30,diccionario.recuperar(3));
		assertEquals(40,diccionario.recuperar(4));
		assertEquals(10,diccionario.recuperar(1));
	}

	@Test
	public void testClaves()
	{
		DiccionarioSimpleTDA diccionario = new DiccionarioSimpleEstatico();
		diccionario.inicializar();
		
		assertTrue(diccionario.claves().estaVacio());
		
		diccionario.agregar(5,50);
		diccionario.agregar(6,60);
		diccionario.agregar(7,70);
		diccionario.agregar(7,71);
		
		assertFalse(diccionario.claves().estaVacio());
		
		int cantidadClaves = 0;
		ConjuntoTDA claves = diccionario.claves();
		
		assertFalse(claves.pertenece(567));
		
		while (!claves.estaVacio())
		{
			int clave = claves.elegir();
			
			assertTrue(claves.pertenece(clave));
			claves.sacar(clave);
			cantidadClaves++;
		}
		
		assertEquals(3,cantidadClaves);
	}
}
