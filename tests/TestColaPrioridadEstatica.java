package tests;

import static org.junit.Assert.*;
import implementaciones.ColaPrioridadEstatica;

import org.junit.Test;

import tdas.ColaPrioridadTDA;

public class TestColaPrioridadEstatica
{
	
	@Test
	public void testInicializar()
	{
		ColaPrioridadTDA cola = new ColaPrioridadEstatica();
		cola.inicializar();
		assertTrue(cola.estaVacia());
	}

	@Test
	public void testAcolarPrioridad()
	{
		ColaPrioridadTDA cola = new ColaPrioridadEstatica();
		cola.inicializar();
		
		cola.acolarPrioridad(900,9);
		cola.acolarPrioridad(100,1);
		cola.acolarPrioridad(500,5);
		
		assertFalse(cola.estaVacia());
	}

	@Test
	public void testDesacolar()
	{
		ColaPrioridadTDA cola = new ColaPrioridadEstatica();
		cola.inicializar();
		
		cola.acolarPrioridad(900,9);
		cola.acolarPrioridad(100,1);
		cola.acolarPrioridad(500,5);
		
		while (!cola.estaVacia())
		{
			cola.desacolar();
		}
		
		assertTrue(cola.estaVacia());
	}

	@Test
	public void testPrimero()
	{
		ColaPrioridadTDA cola = new ColaPrioridadEstatica();
		cola.inicializar();
		
		cola.acolarPrioridad(100,1);
		cola.acolarPrioridad(500,5);
		cola.acolarPrioridad(900,9);
		
		assertEquals(900, cola.primero());
	}

	@Test
	public void testPrioridad()
	{
		ColaPrioridadTDA cola = new ColaPrioridadEstatica();
		cola.inicializar();
		
		cola.acolarPrioridad(100,1);
		cola.acolarPrioridad(900,9);
		cola.acolarPrioridad(500,5);
		
		assertEquals(9, cola.prioridad());
	}

	@Test
	public void testEstaVacia()
	{
		ColaPrioridadTDA cola = new ColaPrioridadEstatica();
		cola.inicializar();
		
		assertTrue(cola.estaVacia());
	}
	
	@Test
	public void testAIgualPrioridadElElementoQueEntroAntesEsElPrimero()
	{
		ColaPrioridadTDA cola = new ColaPrioridadEstatica();
		cola.inicializar();
		
		cola.acolarPrioridad(100,1);
		cola.acolarPrioridad(500,5);
		cola.acolarPrioridad(900,9);
		cola.acolarPrioridad(1000,9);
		
		
		assertEquals(900, cola.primero());
	}

}
