package tests;

import static org.junit.Assert.*;
import implementaciones.ColaPrioridadDinamica;

import org.junit.Test;

import algoritmos.EjerciciosColaPrioridad;

import tdas.ColaPrioridadTDA;
import tdas.ConjuntoTDA;
import tdas.DiccionarioMultipleTDA;

public class TestEjerciciosColaPrioridad {

	@Test
	public void testSonIdenticas() 
	{
		ColaPrioridadTDA cola1 = new ColaPrioridadDinamica();
		cola1.inicializar();
		
		cola1.acolarPrioridad(5,9);
		cola1.acolarPrioridad(7,8);
		cola1.acolarPrioridad(5,7);
		cola1.acolarPrioridad(45,7);
		cola1.acolarPrioridad(7,1);
		
		ColaPrioridadTDA cola2 = new ColaPrioridadDinamica();
		cola2.inicializar();
		
		cola2.acolarPrioridad(5,9);
		cola2.acolarPrioridad(7,8);
		cola2.acolarPrioridad(5,7);
		cola2.acolarPrioridad(45,7);
		cola2.acolarPrioridad(7,1);
		
		assertTrue(EjerciciosColaPrioridad.sonIdenticas(cola1, cola2));
		
		cola1.inicializar();
		cola1.acolarPrioridad(5,9);
		cola1.acolarPrioridad(7,8);
		cola1.acolarPrioridad(5,7);
		
		cola2.inicializar();
		cola2.acolarPrioridad(5,9);
		cola2.acolarPrioridad(7,8);
		cola2.acolarPrioridad(5,7);
		cola2.acolarPrioridad(76,89);
		
		assertFalse(EjerciciosColaPrioridad.sonIdenticas(cola1, cola2));
	}

	@Test
	public void testValorAsociadoAPrioridades()
	{
		ColaPrioridadTDA cola = new ColaPrioridadDinamica();
		cola.inicializar();
		
		cola.acolarPrioridad(5,9);
		cola.acolarPrioridad(7,8);
		cola.acolarPrioridad(5,7);
		cola.acolarPrioridad(45,7);
		cola.acolarPrioridad(7,1);
		
		DiccionarioMultipleTDA d = EjerciciosColaPrioridad.valorAsociadoAPrioridades(cola);
		
		ConjuntoTDA claves = d.claves();
		assertFalse(claves.estaVacio());
		assertTrue(claves.pertenece(5));
		assertTrue(claves.pertenece(7));
		assertTrue(claves.pertenece(45));
		
		ConjuntoTDA valores = d.recuperar(5);
		assertFalse(valores.estaVacio());
		assertTrue(valores.pertenece(9));
		assertTrue(valores.pertenece(7));
	}

}
