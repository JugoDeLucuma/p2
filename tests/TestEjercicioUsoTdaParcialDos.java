package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import algoritmos.EjerciciosUsoTdaParcialDos;
import implementaciones.ABB;
import implementaciones.ColaDinamica;
import implementaciones.ConjuntoDinamico;
import tdas.ABBTDA;
import tdas.ColaPrioridadTDA;
import tdas.ColaTDA;
import tdas.ConjuntoTDA;

public class TestEjercicioUsoTdaParcialDos {

	@Test
	public void testA() 
	{
		ColaTDA cola = new ColaDinamica();
		cola.inicializar();
		
		cola.acolar(7);
		cola.acolar(9);
		cola.acolar(5);
		cola.acolar(7);
		cola.acolar(9);
		cola.acolar(11);
		cola.acolar(5);
		cola.acolar(5);
		
		ConjuntoTDA c = new ConjuntoDinamico();
		c.inicializar();
		
		c.agregar(9);
		c.agregar(11);
		c.agregar(5);
		
		ColaPrioridadTDA cp = EjerciciosUsoTdaParcialDos.a(cola, c);
		
		assertEquals(5, cp.primero());
		assertEquals(3, cp.prioridad());
		
		cp.desacolar();
		
		assertEquals(9, cp.primero());
		assertEquals(2, cp.prioridad());
		
		cp.desacolar();
		
		assertEquals(11, cp.primero());
		assertEquals(1, cp.prioridad());
		
		cp.desacolar();
		
		assertTrue(cp.estaVacia());
	}

	@Test
	public void testSumaElementosProfundidadN()
	{
		ABBTDA a = new ABB();
		a.inicializar();
		
		a.agregar(10);
		a.agregar(15);
		a.agregar(5);
		a.agregar(1);
		a.agregar(7);
		a.agregar(11);
		a.agregar(25);
		a.agregar(6);
		a.agregar(20);
		a.agregar(27);
		a.agregar(18);
		
		assertEquals(10,EjerciciosUsoTdaParcialDos.sumaElementosProfundidadN(a,0));
		assertEquals(20,EjerciciosUsoTdaParcialDos.sumaElementosProfundidadN(a,1));
	}

	@Test
	public void testSumaValoresNivelesIgual()
	{
		ABBTDA a = new ABB();
		a.inicializar();
		
		a.agregar(10);
		a.agregar(15);
		a.agregar(5);
		a.agregar(16);
		a.agregar(4);
		a.agregar(2);
		
		assertTrue(EjerciciosUsoTdaParcialDos.sumaValoresNivelesIgual(a, 1, 2));
		assertFalse(EjerciciosUsoTdaParcialDos.sumaValoresNivelesIgual(a, 2, 3));
	}

}
